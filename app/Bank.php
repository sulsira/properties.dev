<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model {

	protected $fillable = ['name', 'number'];
    public function getFields(){
        return $this->fillable;
    }



}
