<?php namespace App\Commands;

abstract class Command {

    protected  function setupDate($date){
        extract($date);
        if(empty($day) or empty($m) or empty($y)) return '';
        return $m.'-'.$day.'-'.$y;
    }

    /**
     * it would prepare and data fields from the ui form to compatable database field names
     * @param array $data
     * @return array
     */
    protected  function dataMapper(array $data){
        $entity = [];
        foreach($data as $key=>$value){
            if(!empty($key) and !is_numeric($key)){
                if( is_array($value) ){
                    if($key == 'dob'){
                        //input day - month - year
                        $entity['dob'] = $value[2].'-'.$value[1].'-'.$value[0];

                        //output year-month-day
                    }
                }else{

                    $entity[$key] = $value; #it can take when value is 0 string // that loop hole
                }

            }

        }
        return $entity;
    }

    /**
     */
    protected  function databaseTableFields(){
        $set = [];
        if(!empty($this->model->getFields())){
            foreach($this->model->getFields() as $value){
                $set[$value] = $value;
            }
        }
        return $set;
    }
}
