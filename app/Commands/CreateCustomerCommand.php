<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateCustomerCommand extends Command {
    public $person,$contacts, $payment, $customer, $document, $image;
    /**
     * Create a new command instance.
     *
     * @return \App\Commands\CreateCustomerCommand
     */
	public function __construct($pers_fname, $pers_mname, $pers_lname, $contacts, $customer, $payment, $m,$f,$o,$r)
	{
        $dateOfPayment = ($this->setupDate($f))?: $this->setupDate($m);
        $dateOfPayment = ($dateOfPayment)?:  $this->setupDate($r);
        $dateOfPayment = ($dateOfPayment)?:$this->setupDate($o);
        $this->person = $this->mapPersonsTable(
            compact('pers_fname',
                'pers_mname',
                'pers_lname'
            )
        );

        $payment_details = array_add($payment, 'dateOfPayment', $dateOfPayment);
        $this->contacts = $this->dataMapper($contacts);
        $this->customer = $this->dataMapper($customer);
        $this->payment = $payment_details;

        $this->document = \Input::file('document');
        $this->image = \Input::file('photo');
	}
    private function mapPersonsTable(array $data){
        $person = [];
        foreach($data as $key=>$value){
            if(!empty($key) and !is_numeric($key)){
                if( is_array($value) ){
                    if($key == 'dob'){
                        //input day - month - year
                        $person['pers_DOB'] = $value[2].'-'.$value[1].'-'.$value[0];
                        //output year-month-day
                    }
                }else{
                    $person[$key] = $value; #it can take when value is 0 string // that loop hole
                }
            }

        }
        return $person;
    }
}
