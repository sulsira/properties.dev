<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use Realestate\Account\Expense\ExpenseRepositoryInterface;

class CreateExpenseCommand extends Command implements SelfHandling {

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
    public function __construct($description, $category, $total, $f)
    {
        $this->category = $category;
        $this->description = $description;
        $this->total = $total;
        $this->date = $this->setupDate($f);
    }

    /**
     * Execute the command.
     *
     * @param ExpenseRepositoryInterface $incomeRepository
     * @return void
     */
    public function handle(ExpenseRepositoryInterface $expenseRepository)
    {
        $data = $this->prepared();

        $expenseObject = $expenseRepository->creation($data);

        $saved = $expenseRepository->save( $expenseObject );

//        \Event::fire(new IncomeAdded(User, incomeObject));

        return $expenseObject;
    }
    private function prepared(){

        return [
            'status' => 0 ,
            'description' => $this->description,
            'category' => $this->category,
            'total' => $this->total,
            'approved'=> 'not-approved',
            'date' => $this->date
        ];
    }

}
