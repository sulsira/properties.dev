<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use Realestate\Account\Income\IncomeRepository;

class CreateIncomeCommand extends Command implements SelfHandling {

    public $description, $category, $total, $date;
    /**
     * Create a new command instance.
     *
     * @param $description
     * @param $category
     * @param $total
     * @param $f
     * @return \App\Commands\CreateIncomeCommand
     */
	public function __construct($description, $category, $total, $f)
	{
		$this->category = $category;
        $this->description = $description;
        $this->total = $total;
        $this->date = $this->setupDate($f);
	}

    /**
     * Execute the command.
     *
     * @param IncomeRepository $incomeRepository
     * @internal param $command
     * @return void
     */
	public function handle(IncomeRepository $incomeRepository)
	{
        $data = $this->prepared();

        $incomeObject = $incomeRepository->creation($data);

        $saved = $incomeRepository->save( $incomeObject );

//        \Event::fire(new IncomeAdded(User, incomeObject));

        return $incomeObject;
	}
    private function prepared(){

        return [
            'status' => 0 ,
            'description' => $this->description,
            'category' => $this->category,
            'total' => $this->total,
            'approved'=> 'not-approved',
            'date' => $this->date
        ];
    }
}
