<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateInvoiceCommand extends Command {

    public $description, $customers, $total, $quantity, $due_date, $product, $issued_date;
    /**
     * Create a new command instance.
     *
     * @return \App\Commands\CreateInvoiceCommand
     */
	public function __construct($description, $customers, $total, $quantity,$product, $d, $i)
	{
        $this->description = $description;
        $this->customers = $customers;
        $this->total = $total;
        $this->quantity = $quantity;
        $this->due_date = $this->setupDate($d);
        $this->issued_date = $this->setupDate($i);
        $this->product = $product;

	}


}
