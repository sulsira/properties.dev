<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreatePlotCustomerCommand extends Command {
public $person, $next_kin, $contacts, $plot_payment, $customer, $document, $image;
    /**
     * Create a new command instance.
     *
     * @return \App\Commands\CreatePlotCustomerCommand
     */
public function __construct(

                            $pers_fname,
                            $pers_mname,
                            $pers_lname,
                            $day,
                            $month,
                            $year,
                            $pers_gender,
                            $pers_nationality,
                            $customerType,
                            $paymentPeriodInMonths,
                            $pers_indentifier,
                            $fullname,
                            $contacts,
                            $contact,
                            $plot_id,
                            $downPayment,
                            $paymentCommencementDate,
                            $amountPaid,
                            $duePayment,
                            $m,$f

                            )
{       

        $dob = [$day,$month,$year];

        $dateOfPayment = ($this->setupDate($f))?: $this->setupDate($m);

        $pers_type = 'plot_customer';
        $totalPayment = $amountPaid;
        $paymentType =& $customerType;
        $this->person = $this->mapPersonsTable(
            compact('pers_fname',
                'pers_mname',
                'pers_lname',
                'dob',
                'pers_gender',
                'pers_nationality',
                'pers_indentifier',
                'dob',
                'pers_type'
            )
        );
        $this->next_kin = $this->dataMapper(compact('fullname','contacts'));
        $this->contacts = $this->dataMapper($contact);
        $this->customer = $this->dataMapper(compact('customerType','plot_id'));
        $this->plot_payment = $this->dataMapper(compact('paymentPeriodInMonths','paymentType','paymentStatus','paymentCommencementDate','downPayment','totalPayment','dateOfPayment','duePayment'));
        $this->document = \Input::file('document');
        $this->image = \Input::file('photo');
}



    private function mapPersonsTable(array $data){
        $person = [];
        foreach($data as $key=>$value){
            if(!empty($key) and !is_numeric($key)){
                if( is_array($value) ){
                    if($key == 'dob'){
                        //input day - month - year
                        $person['pers_DOB'] = $value[2].'-'.$value[1].'-'.$value[0];
                        //output year-month-day
                    }
                }else{
                    $person[$key] = $value; #it can take when value is 0 string // that loop hole
                }
            }

        }
       return $person;
    }


}
