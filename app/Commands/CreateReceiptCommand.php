<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use Input;

class CreateReceiptCommand extends Command {

    public $file, $description, $account, $category, $total, $date, $reference;

    /**
     * Create a new command instance.
     *
     * @param $description
     * @param $account
     * @param $category
     * @param $total
     * @param $f
     * @return \App\Commands\CreateReceiptCommand
     */
	public function __construct($description, $account, $category, $total, $f, $reference)
	{
        $this->file = \Input::file('upload');
        $this->description = $description;
        $this->account = $account;
        $this->category = $category;
        $this->total = $total;
        $this->date = $this->setupDate($f);
        $this->reference = $reference;

	}



}
