<?php namespace App\Commands;

use App\Commands\Command;

use App\Transaction;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateTransactionCommand extends Command {
    protected $input;
    protected $model;
//public $title,$description,$revenue,$receipt_number,$cheque_reference,$bank_number,$bank_account,$category,$customer,$total,$for,$dateofpayment;
    /**
     *
     * Create a new command instance.
     *
     * @param $title
     * @param $description
     * @param $revenue
     * @param $account
     * @param $receipt_number
     * @param $cheque_reference
     * @param $bank_number
     * @param $bank_account
     * @param $category
     * @param $customer
     * @param $total
     * @param $for
     * @param $f
     * @param $p
     * @param $r
     * @param $o
     * @return \App\Commands\CreateTransactionCommand
     */
	public function __construct($title, $description, $revenue, $account, $receipt_number, $cheque_reference, $bank_number, $bank_account, $category, $customer, $total, $for,$f, $p, $r,$o)
	{
        $this->model = new Transaction();
        $this->input = \Input::all();
        $this->data = $this->fieldsMapInputs(compact( 'title', 'description', 'revenue', 'account', 'receipt_number', 'cheque_reference', 'bank_number', 'bank_account', 'category', 'customer', 'total', 'for','f', 'p', 'r','o'));
        $this->upload = \Input::file('upload');
	}

    private function fieldsMapInputs($data){
        $results = [];
        extract($this->databaseTableFields());


        $results[$title] = $data['title'];
        $results[$payment_for] = $data['for'];
        $results[$revenue] = $data['revenue'];

        $results[$category] = $data['category'];
        $results[$description] = $data['description'];
        $results[$paymentMedium] = $data['account'];
        $results[$customer_id] = $data['customer'];
        $results[$total] = $data['total'];
        $results[$amountPaid] =  $data['total'];
        $results[$approval_status] = '';
        $results[$plot_id] = (isset($this->input['plot_id']))? $this->input['receipt_number'] : '';
        $results[$user_id] = '';
        $date = '';
        $dates = [$data['f'], $data['p'], $data['r'],$data['o']];
        foreach($dates as $key => $value){
            if(!empty($this->setupDate($value))){
                $date = $this->setupDate($value);
            }
        }
        $results[$paymentDate] = $date;
        $results[$documentUrl] = '';

        if($results[$paymentMedium] == 'cash'){

            $results[$receipt_number] = (isset($this->input['receipt_number']))? $this->input['receipt_number'] : $data['receipt_number'];
            $results[$paymentMedium] = ['account'=>$data['account'], 'receipt_number' => $results[$receipt_number]];
        }elseif($results[$paymentMedium] == 'bank'){

            $results[$bankable_id] = $data['bank_number'];
            $results[$paymentMedium] = ['account'=>$data['account'], 'bankable_id' => $results[$bankable_id]];

        }elseif($results[$paymentMedium] == 'cheque'){

            $results[$bankable] = $data['bank_account'];

            $results[$bank_cheque] = $data['cheque_reference'];
            $results[$paymentMedium] = ['account'=>$data['account'], 'bank_cheque'=> $results[$bank_cheque], 'bankable_id'=>$results[$bankable] ];
        }

        return  $results ;

        //
        //		$this->description = $description;
        //
        //		$this->revenue = $revenue;
        //
        //		$this->account = $account;
        //
        //		$this->receipt_number = $receipt_number;
        //
        //		$this->cheque_reference = $cheque_reference;
        //
        //		$this->bank_number = $bank_number;
        //
        //		$this->bank_account = $bank_account;
        //
        //		$this->category = $category;
        //
        //		$this->customer = $customer;
        //
        //		$this->total = $total;
        //
        //		$this->for = $for;


        //
        //		$this->f = $f;
        //
        //		$this->p = $p;
        //
        //		$this->r = $r;
        //
        //		$this->o =$o;
    }

}
