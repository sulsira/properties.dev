<?php namespace App\Commands;

//use ;

use Illuminate\Queue\InteractsWithQueue;
use Request;

class CreateUserCommand extends Command{

    public $user;
    public $role;
    public $property;
    public  $input;

    function __construct($input)
    {
        $this->input = $input ?: Request::all();
        $this->user = $this->user();
        $this->role = $this->role();
    }

    private function user(){
        $data  = [
            'email' => $this->input['email'],
            'password'=> $this->input['password']
        ];
        return $data;
    }
    private function role(){
        $data = [
            'security_level' => $this->input['security_level'],
            'domain' => $this->input['domain'],
            'usergroup' => $this->input['user_group'],
            'fullname' => $this->input['first_name'].' '.$this->input['last_name'],
            'department' => $this->input['department'],
            'phone' => $this->input['phones'],
            'previledges' => $this->input['previledges'],
            'profilePic' => Request::file('profilePic')        ];
        return  $data;
    }



}
