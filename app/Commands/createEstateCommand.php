<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class createEstateCommand extends Command {
    public $name;
    public $location;
    /**
     * Create a new command instance.
     *
     * @return \App\Commands\createEstateCommand
     */
	public function __construct($name, $location)
	{
		$this->name = $name;
        $this->location = $location;
	}


}
