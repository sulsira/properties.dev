<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreatePlotCommand extends Command{

    public $estate_id, $plot_customer_id, $agent_id, $indentification, $size, $price, $availability, $purchaseType, $remarks;

    /**
     * Create a new command instance.
     *
     * @param $estate_id
     * @param $indentification
     * @param $size
     * @param $price
     * @param $availability
     * @param $purchaseType
     * @param $remarks
     * @internal param $plot_customer_id
     * @internal param $agent_id
     * @return \App\Commands\createPlotCommand
     */
	public function __construct( $estate_id, $indentification, $size, $price, $availability, $purchaseType, $remarks)
	{
		$this->estate_id = $estate_id;
//        $this->plot_customer_id = $plot_customer_id ;$plot_customer_id, $agent_id,
//        $this->agent_id = $agent_id ;$plot_customer_id, $agent_id,
        $this->indentification = $indentification ;
        $this->size = $size ;
        $this->price = (float)$price;
        $this->availability = $availability ;
        $this->purchaseType = $purchaseType ;
        $this->remark = $remarks ;
	}


}
