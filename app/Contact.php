<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

	protected $table = 'contacts';
    protected $primaryKey = 'id';
    protected $fillable =['Cont_ContactInfoID', 'Cont_EntityID', 'Cont_EntityType', 'Cont_Contact', 'Cont_ContactType', 'Cont_Deleted'];

    public function person(){
        return $this->belongsTo('App\Person','id','Cont_EntityID');
    }

    public function scopePersontype($query){
        return $query->where('Cont_EntityType', '=','person');
    }
}
