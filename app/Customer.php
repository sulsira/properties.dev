<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {
    protected $fillable = ['customerType', 'paymentType'];
    public function getFields(){
        return $this->fillable;
    }

    public function customable(){
        return $this->morphTo();
    }

    public function transactions(){
        return $this->hasMany('App\Transaction','customer_id', 'id');
    }


}
