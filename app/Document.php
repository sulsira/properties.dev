<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model {

        protected $table = 'documents';
        protected $primaryKey = 'id';
        protected $fillable = [
            'title', 'entity_type', 'entity_ID', 'type', 'fullpath', 'filename', 'foldername', 'extension', 'filetype', 'thumnaildir', 'userID', 'deleted'
        ];
	//

}
