<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Estate extends Model {
	protected $table = 'estates';
	protected $primaryKey = 'id';

	protected $fillable = ['name', 'location'];
	//
    public static function creation($data){

        $data = (array) $data;

        $entity = new static($data);

        return  $entity;

    }
    // scopes and mics
    public function scopeSelect($query){
        return $query->where('name','!=', null)->get()->toArray();
    }
    // Relationships mehtods below:
    public function plots(){
        return $this->hasMany('App\Plot');
    }
    //end of relationship methods


}
