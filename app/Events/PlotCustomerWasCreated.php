<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class PlotCustomerWasCreated extends Event {

	use SerializesModels;
    public $plotcustomerId;
    public $plotId;

    /**
     * Create a new event instance.
     *
     * @return \App\Events\PlotCustomerWasCreated
     */
	public function __construct($plotcustomerId, $plotId)
	{
        $this->plotcustomerId = $plotcustomerId;
        $this->plotId = $plotId;
	}

}
