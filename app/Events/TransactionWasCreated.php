<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class TransactionWasCreated extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
       //we want the:
        //category
        //accont:income, expense,
        //who made the payment
        // the title or description
        //total of payment
        //dont forget to match in the event Service provider
        // then we want to call the handler to do the rest
        //that is to do the following:

            // store in the notification table

            // then push it to pusher
	}

}
