<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model {

    protected $fillable = [
        'status',
        'description',
        'category',
        'total',
        'approved',
        'date'
    ];


    public function setDateAttribute($date){
        $this->attributes['date'] = Carbon::parse($date);
    }
}
