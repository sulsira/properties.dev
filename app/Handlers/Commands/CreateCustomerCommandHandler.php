<?php namespace App\Handlers\Commands;



use Illuminate\Queue\InteractsWithQueue;
use Realestate\Account\Customer\CustomerRepositoryInterface;
use Realestate\Account\Transaction\TransactionRepositoryInterface;
use Realestate\Document\DocumentRepositoryInterface;
use Realestate\Person\Contact\ContactRepositoryInterface;
use Realestate\Person\PersonRepositoryInterface;
use Realestate\Person\Picture\PictureRepositoryInterface;
use Realestate\Plot\Customer\CustomerRepository as plotscustomer;
use Realestate\Plot\Kin\NextKinRepositoryInterface;
use Realestate\Plot\Payment\PaymentRepositoryInterface;

class CreateCustomerCommandHandler {
    public $personRepo;
    public $customerRepo;
    public $plotpaymentRepo;
    public $contactRepo;
    public $nextkinRepo;
    public $pictureRepo;
    public $documentRepo;
    public $plotcustomerRepo;
    public $transactionRepository;

    /**
     * Create the command handler.
     *
     * @param PersonRepositoryInterface $personRepo
     * @param CustomerRepositoryInterface $customerRepo
     * @param PaymentRepositoryInterface $paymentRepo
     * @param NextKinRepositoryInterface $nextkinRepo
     * @param ContactRepositoryInterface $contactRepo
     * @param PictureRepositoryInterface $pictureRepo
     * @param DocumentRepositoryInterface $documentRepo
     * @param CustomerRepositoryInterface|plotscustomer $plotcustomerRepo
     * @param TransactionRepositoryInterface $transactionRepository
     * @return \App\Handlers\Commands\CreateCustomerCommandHandler
     */
	public function __construct(

        PersonRepositoryInterface $personRepo,
        CustomerRepositoryInterface $customerRepo,
        PaymentRepositoryInterface $paymentRepo,
        NextKinRepositoryInterface $nextkinRepo,
        ContactRepositoryInterface $contactRepo,
        PictureRepositoryInterface $pictureRepo,
        DocumentRepositoryInterface $documentRepo,
        plotscustomer $plotcustomerRepo,
        TransactionRepositoryInterface $transactionRepository


    )
	{
        $this->personRepo = $personRepo;
        $this->customerRepo = $customerRepo;
        $this->plotpaymentRepo = $paymentRepo;
        $this->nextkinRepo = $nextkinRepo;
        $this->contactRepo = $contactRepo;
        $this->pictureRepo = $pictureRepo;
        $this->documentRepo = $documentRepo;
        $this->plotcustomerRepo = $plotcustomerRepo;
        $this->transactionRepository = $transactionRepository;
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle( $command)
	{


        // first we add to persons
        $created_customer = [];

        $personObject = $this->personRepo->creation($command->person);
        $this->personRepo->save($personObject);

        $customer = $this->addCustomer($command, $personObject->id);

//        then add to transactions with domain payments accounts

        $this->makePayments($command, $customer);

        // if adding is done and plots or houses where involve touch the plot or house

        // send notification and email if email was added

        // push event and done


        $contactObject = $this->addContact($command, ['Cont_EntityID'=>$personObject->id, 'Cont_EntityType'=>'Person']);

//        event(new PlotCustomerWasCreated(1,2));
//        \Event::fire(new AccountCustomerWasCreated(1,2));
        return $customer;
	}


    /**
     * adding plot customer data
     * @param $command
     * @param $person_id
     * @internal param $
     * @return array
     */

    private function addCustomer($command, $person_id){
//            add to domain customer, then to customable customers table
        $customer = $command->customer;

        extract($customer);

        if($customer['customer_type'] == 'fullpayment' or $customer['customer_type'] == 'mortgage'){

            $plotCustomerObj = $this->plotcustomerRepo->creation(['person_id'=>$person_id,'customerType'=>$customer_type,'status'=>1 ]);

            $plotcustomer = $this->plotcustomerRepo->save($plotCustomerObj);

            $customer = $this->plotcustomerRepo->customers()->create(['customerType'=>$customer_type]);


        }
        if($customer['customer_type'] == 'rent'){

            $domain =  'App\PlotsCustomer';

            $customer = $this->customerRepo->creation(['customerType'=>$customer_type, 'customable_type'=>$domain, '0']);

            // right now the rent is not implemented

            // so a defual quick fix is assigned

            // now dont create anything
        }

        return $customer;
    }

    private function addPlotPayment($command, $ids){
        $payment_details = [];
        $customer = $command->customer;
        $payment = $command->payment;

        extract($ids);
        $payment_details['plot_customer_id'] = $customer_id;
        $payment_details['plot_id'] = $plot_id;
        $payment_details['paymentType'] = $customer['payment_type'];
        $payment_details['transaction'] = $customer['account'] ;
        $payment_details['dateOfPayment'] = $payment['dateOfPayment'] ;

//        dateOfPayment

        if($customer['customer_type'] == 'fullpayment'){

            $payment_details['paymentStatus'] = 'closed';
            $payment_details['totalPayment'] = $payment['Plot_fullpayment']['amountPaid'];


        }

        if( $customer['customer_type'] == 'mortgage'){
            $payment_details['paymentStatus'] = 'open';
            $payment_details['downPayment'] = $payment['Plot_mortgage']['down_payment'];
            $payment_details['duePayment'] = $payment['Plot_mortgage']['due_payment'];
            $payment_details['paymentPeriodInMonths'] = $payment['Plot_mortgage']['payment_period_in_months'];
            $payment_details['paymentCommencementDate'] = $payment['Plot_mortgage']['payment_commencement_date'];

        }


        $ready_data = $this->mapping($payment_details, $customer['customer_type']);
        $paymentObject = $this->plotpaymentRepo->creation($ready_data);
        if(empty($paymentObject)) return [];
        $payment = $this->plotpaymentRepo->save($paymentObject);
        return  $paymentObject;

    }

    private function addContact($command, $entity){

        $contact = $command->contacts;
        extract($entity);

        $contact = array_add($contact, 'Cont_EntityID', $Cont_EntityID);
        $contact = array_add($contact, 'Cont_EntityType', $Cont_EntityType);

        $contactObject = $this->contactRepo->creation($contact);

        return $contactObject;
    }



    public function makePayments($command, $customer){

      $transactions = [];
        $customer_input = $command->customer;
        $payment = $command->payment;


        if($customer_input['customer_type'] == 'fullpayment' or $customer_input['customer_type'] == 'mortgage'){
//            $the_plot = 'Plot_'.$customer_input['customer_type'];
//            extract($payment[$the_plot]);
            $plot_id  = ($customer_input['customer_type'] == 'fullpayment')? $payment['Plot_fullpayment']['plot_id'] : $payment['Plot_mortgage']['plot_id'] ;

            $paymentObject = $this->addPlotPayment($command, ['plot_id'=> $plot_id , 'customer_id'=> $customer->id]);
            $amount_paid = ($payment['Plot_fullpayment']['amount_paid'])?: $payment['Plot_mortgage']['down_payment'];
            $transactions = [

                'revenue' => $customer_input['account'],
                'receipt_number' => $customer_input['receipt_number'] ,
                'bankable' => $customer_input['bank_account'] ,
                'bankable_id' => $customer_input['cheque_bank_account'] ,
                'bank_cheque' => $customer_input['cheque'],
                'category' => $customer_input['customer_type'],
                'description' => $payment['Plot_fullpayment']['description'] ,
                'amountPaid' => $amount_paid,
                'paymentDate' => $payment['dateOfPayment'],
                'paymentMedium' => $customer_input['payment_type'],
                'customer_id' => $customer->id,
                'plot_id' => $plot_id
            ];



        }


        if($customer_input['customer_type'] == 'rent'){

            $transactions = [
                'revenue' => $customer_input['account'],
                'receipt_number' => $customer_input['receipt_number'] ,
                'bankable' => $customer_input['bank_account'] ,
                'bankable_id' => $customer_input['cheque_bank_account'] ,
                'bank_cheque' => $customer_input['cheque'],
                'category' => $customer_input['customer_type'],
                'description' => $payment['rent']['description'] ,
                'amountPaid' =>  $payment['rent']['payment'],
                'paymentDate' => $payment['dateOfPayment'],
                'paymentMedium' => $customer_input['payment_type'],
                'customer_id' => $customer->id,
            ];
        }

        if($customer_input['customer_type'] == 'others'){
//            $this->rentPaymentRepository->creation(['']);
            $transactions = [
                'payment_for' =>  $payment['others']['for'],
                'revenue' => $customer_input['account'],
                'receipt_number' => $customer_input['receipt_number'] ,
                'bankable' => $customer_input['bank_account'] ,
                'bankable_id' => $customer_input['cheque_bank_account'] ,
                'bank_cheque' => $customer_input['cheque'],
                'category' => $customer_input['customer_type'],
                'description' => $payment['others']['description'] ,
                'amountPaid' => $payment['others']['payment'],
                'paymentDate' => $payment['dateOfPayment'],
                'paymentMedium' => $customer_input['payment_type'],
            ];


        }

       $transactions =  $this->transactionRepository->creation($transactions);;
        $this->transactionRepository->save($transactions);
        return $transactions;

    }

    private function mapping($data, $type = null){
//        $map = $this->plotpaymentRepo->setDataMapper();
        extract($data);


        $map_set = [];

        $map_set['paymentPeriodInMonths'] = $paymentPeriodInMonths;
        $map_set['paymentCommencementDate'] = $paymentCommencementDate;
        $map_set['totalPayment'] = (isset($totalPayment))? $totalPayment : 0;
        $map_set['downPayment'] = $downPayment;
        $map_set['dateOfPayment'] = $dateOfPayment;
        $map_set['duePayment'] = $duePayment;
        $map_set['plot_customer_id'] = $plot_customer_id;
        $map_set['plot_id'] = $plot_id;
        $map_set['paymentType'] = $type;
        $map_set['transaction'] = $transaction;
        $map_set['paymentStatus'] = $paymentStatus;
        $map_set['paymentStatus'] = $paymentStatus;

      return $map_set;
    }


}
