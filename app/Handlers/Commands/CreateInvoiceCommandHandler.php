<?php namespace App\Handlers\Commands;


use Illuminate\Queue\InteractsWithQueue;
use Realestate\Account\Invoice\InvoiceRepositoryInterface;

class CreateInvoiceCommandHandler {

    protected  $invoiceRepository;

    public function __construct(InvoiceRepositoryInterface $invoiceRepository){
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * Handle the command.
     *
     * @param    $command
     * @internal param InvoiceRepositoryInterface $invoiceRepository
     * @return void
     */
	public function handle($command)
	{
        $data = $this->prepareInvoiceData($command);

        $invoiceObject = $this->invoiceRepository->creation($data);
//
        $saved_invoice = $this->invoiceRepository->save($invoiceObject);

//        dd($saved_invoice);
//        $this->notifyInvoiceCreation($saved_invoice);
//
        return $invoiceObject;
	}


    private function prepareInvoiceData($command){

        extract((array)$command);
        return [
            "productService" => $product,
            "description" =>  $description,
            "quantity" => $quantity,
            "amount" => $total,
            "dueDate" => $due_date,
            "issuedDate" => $issued_date
        ];
    }

}
