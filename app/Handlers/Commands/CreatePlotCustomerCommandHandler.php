<?php namespace App\Handlers\Commands;

//use ;

use App\Events\PlotCustomerWasCreated;
use App\Person;
use App\Plot;
use App\PlotsCustomer;
use Illuminate\Queue\InteractsWithQueue;

use Realestate\Document\DocumentRepositoryInterface;
use Realestate\Person\Contact\ContactRepositoryInterface;
use Realestate\Person\Contact\ContractRepositoryInterface;
use Realestate\Person\PersonRepositoryInterface;
use Realestate\Person\Picture\PictureRepositoryInterface;
use Realestate\Plot\Customer\CustomerRepositoryInterface;
use Realestate\Plot\Kin\NextKinRepositoryInterface;
use Realestate\Plot\Payment\PaymentRepositoryInterface;
use Sulsira\Uploader\Uploader;

class CreatePlotCustomerCommandHandler {

    public $personRepo;
    public $customerRepo;
    public $plotpaymentRepo;
    public $contactRepo;
    public $nextkinRepo;
    public $pictureRepo;
    public $documentRepo;

    /**
     * Create the command handler.
     *
     * @param PersonRepositoryInterface $personRepo
     * @param CustomerRepositoryInterface $customerRepo
     * @param PaymentRepositoryInterface $paymentRepo
     * @param NextKinRepositoryInterface $nextkinRepo
     * @param ContactRepositoryInterface $contactRepo
     * @param PictureRepositoryInterface $pictureRepo
     * @param DocumentRepositoryInterface $documentRepo
     * @return \App\Handlers\Commands\CreatePlotCustomerCommandHandler
     */
	public function __construct(
        PersonRepositoryInterface $personRepo,
        CustomerRepositoryInterface $customerRepo,
        PaymentRepositoryInterface $paymentRepo,
        NextKinRepositoryInterface $nextkinRepo,
        ContactRepositoryInterface $contactRepo,
        PictureRepositoryInterface $pictureRepo,
        DocumentRepositoryInterface $documentRepo
    )
	{
		$this->personRepo = $personRepo;
        $this->customerRepo = $customerRepo;
        $this->plotpaymentRepo = $paymentRepo;
        $this->nextkinRepo = $nextkinRepo;
        $this->contactRepo = $contactRepo;
        $this->pictureRepo = $pictureRepo;
        $this->documentRepo = $documentRepo;
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle( $command )
	{

        DB::beginTransaction();
        try{

            $created_customer = [];

            $personObject = $this->personRepo->creation($command->person);
            $person = $this->personRepo->save($personObject);

            $customer = $this->addCustomer($command, $personObject->id);



            $paymentObject = $this->addPlotPayment($command, ['plot_id'=>$command->customer['plot_id'], 'customer_id'=> $customer->id]);

            $nextkinObject = $this->addNextKin($command, ['EntityType'=>'PlotsCustomer','EntityID'=>$customer->id] );

            $contactObject = $this->addContact($command, ['Cont_EntityID'=>$personObject->id, 'Cont_EntityType'=>'Person']);

            if( $command->image ){

                $picture = $this->upload_profile_pic($command, $personObject);

                $this->addPicture($command, $picture, $personObject->id);
            }


            if( $command->document ){

                $document = $this->upload_document($command, $personObject);

                $this->addDocument($document);

            }
//        event(new PlotCustomerWasCreated(1,2));
            \Event::fire(new PlotCustomerWasCreated(1,2));

            DB::commit();
            return $customer;
        } catch(Exception $e){
            DB::rollback() ;
        }



//



	}


    /**
     * adding plot customer data
     * @param $command
     * @param $person_id
     * @internal param $
     * @return array
     */

    private function addCustomer($command, $person_id){

        $customer = $command->customer;
        $customer = array_add($customer, 'person_id', $person_id);
        $customerObject =  $this->customerRepo->creation( $customer );
        $customer = $this->customerRepo->save($customerObject);
        Plot::find((int)$customer['plot_id'])->update(['plot_customer_id'=>$customerObject->id]);
       if(!$customer){

           return $customer;

       }
        return $customerObject;
    }

    private function addPlotPayment($command, $ids){
        extract($ids);
        $payment = $command->plot_payment;
        $payment = array_add($payment, 'plot_customer_id', $customer_id);
        $payment = array_add($payment, 'plot_id', $plot_id);

        $paymentObject = $this->plotpaymentRepo->creation($payment);
        if(empty($paymentObject)) return [];
        $payment = $this->plotpaymentRepo->save($paymentObject);

        return  $paymentObject;

    }

    private function addNextKin($command, $entity){


        extract($entity);
        $nextkin = $command->next_kin;

        $nextkin = array_add($nextkin, 'EntityType', $EntityType);
        $nextkin = array_add($nextkin, 'EntityID', $EntityID);
        $kinObject = $this->nextkinRepo->creation($nextkin);
        if(empty($kinObject)) return [];
        $kin = $this->nextkinRepo->save($kinObject);

        return $kinObject;
    }
    private function addContact($command, $entity){
        $contact = $command->contacts;
        extract($entity);

        $contact = array_add($contact, 'Cont_EntityID', $Cont_EntityID);
        $contact = array_add($contact, 'Cont_EntityType', $Cont_EntityType);

        $contactObject = $this->contactRepo->creation($contact);

        return $contactObject;
    }

    private function upload_profile_pic($command, $personObject){

        $name = $personObject->pers_fname.' '.$personObject->pers_mname.' '.$personObject->pers_lname;
        $ran = base_convert(rand(10000,99999), 10, 36);
        $main_dir = 'media/estates/plot_customers/'.$name.'_'.$personObject->id.'/images';

        $img = Uploader::image($command->image)
            ->directory($main_dir)
            ->rename($personObject->pers_fname.'_'.$personObject->id.'_'.$ran)
            ->resize(128,128, 'thumbsnails');
        $image = $img->details();

        return $image;

    }

    private function addPicture($command, $picture, $personObjectid){
        $image = $command->image;
        $data = [
            'name'=>$image->getClientOriginalName(),
            'entity_type'=>'Person',
            'entity_ID'=> $personObjectid,
            'type'=> 'plot_customer_image',
            'fullpath'=>$picture['new']['file'],
            'filename'=>$picture['new']['filename'],
            'foldername' => $picture['new']['folder'],
            'filetype' => $image->getMimeType(),
            'thumnaildir' =>$picture['new']['thumnail_location'],
            'image'=> $picture['new']['file'],
            'extension' => $image->getClientOriginalExtension()
        ];
        $pictureObject = $this->pictureRepo->creation($data);
        $picture = $this->pictureRepo->save($pictureObject);
        return $pictureObject;
    }

    public function upload_document($command, $personObject){
        $document =  $command->document;

        $name = $personObject->pers_fname.' '.$personObject->pers_mname.' '.$personObject->pers_lname;
        $ran = base_convert(rand(10000,99999), 10, 36);
        $main_dir = 'media/estates/plot_customers/'.$name.'_'.$personObject->id.'/documents';

        $data = [
            'filename' =>$document->getClientOriginalName(),
            'extension'=> $document->getClientOriginalExtension(),
            'entityID' => $personObject->id,
            'entityType' => 'Person',
            'folder'=> $main_dir,
            'fileRename' => $personObject->pers_fname.'_'.$personObject->id.'_'.$ran,
            'fileObject'=> $document
        ];
        $uploaded_details = $this->documentRepo->upload( $data );
      return $uploaded_details;
    }

    public function addDocument($document){
        $documentObj = $this->documentRepo->creation($document);
        $this->documentRepo->save($documentObj);
    }
}
