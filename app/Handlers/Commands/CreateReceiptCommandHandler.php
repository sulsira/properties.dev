<?php namespace App\Handlers\Commands;



use Illuminate\Queue\InteractsWithQueue;
use Realestate\Account\Receipt\ReceiptRepositoryInterface;

class CreateReceiptCommandHandler {
    protected  $receiptRepository;
    /**
     * Create the command handler.
     *
     * @param ReceiptRepositoryInterface $receiptRepository
     * @return \App\Handlers\Commands\CreateReceiptCommandHandler
     */
	public function __construct(ReceiptRepositoryInterface $receiptRepository)
	{
        $this->receiptRepository = $receiptRepository;
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle($command)
	{

        $data = $this->prepare($command);

        if($command->file){
            $file_details = $this->upload_file($command->file);
            $data['file'] = $file_details;
        }

		$receiptObject = $this->receiptRepository->creation($data);

        $status  = $this->receiptRepository->save($receiptObject);


        return $receiptObject;

	}

    private function upload_file($file){

        $fixed_dir = 'media\accounts\receipts';

        $file_location = '';

        $mime = $file->getMimeType() ;

        if(! \File::exists($fixed_dir)){

            \File::makeDirectory($fixed_dir,  $mode = 0777, $recursive = true);

        }
        if(\File::isDirectory($fixed_dir)){
            if(! \File::isDirectory($fixed_dir)){
                // the direcotry writeable or something
                // create the directory with privelges
            }
            // prepare information for upload
            $uploaded = $file->move($fixed_dir,'receipt_'.$file->getClientOriginalName());

            if($uploaded){
                $file_location =  $fixed_dir.'\receipt_'.$file->getClientOriginalName();
            }
        }
        return  $file_location;

    }
    private function prepare($command){
        return [
//                'user_id'  => $command->some,
//                'entity_id' => $command->some,
//                'entityType' => $command->some,
                'description' => $command->description,
//                'paymentType' => $command->some,
                'amount' => $command->total,
                'receptDate' => $command->date,
//                'bankAccount_id' => $command->some,
//                'source' => $command->some,
                'account' => $command->account,
                'category' => $command->category,
                'reference' => $command->reference,
                'file'  => ''
        ];
    }
}
