<?php namespace App\Handlers\Commands;



use App\Events\TransactionWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Realestate\Account\Transaction\TransactionRepositoryInterface;
use Realestate\Document\DocumentRepositoryInterface;

class CreateTransactionCommandHandler {

    protected $transaction;
    protected $documentRepo;
    /**
     * Create the command handler.
     *
     * @param TransactionRepositoryInterface $transactionRepo
     * @param DocumentRepositoryInterface $documentRepo
     * @return \App\Handlers\Commands\CreateTransactionCommandHandler
     */
	public function __construct(TransactionRepositoryInterface $transactionRepo, DocumentRepositoryInterface $documentRepo)
	{
		$this->transaction = $transactionRepo;
        $this->documentRepo = $documentRepo;
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle( $command )
	{

       return DB::transaction(function() use($command) {

            $transactionObj = $this->transaction->creation($command);

            $transaction = $this->transaction->save($transactionObj);

            if($transaction){


                if( $command->upload ){

                    $document = $this->upload($command, $transactionObj);

                    $this->addUpload($document);

                }

                // get the user, and transaction,
                $user_id = 0 ;
                \Event::fire(new TransactionWasCreated(  $user_id,  $transactionObj ));
//                push an event of payment

            }
            return $transactionObj;
        });


	}

    public function upload($command, $transactionObj){
        $upload =  $command->upload;
//
        $name = ($transactionObj->title)?: Str::random($length = 16);
        $ran = base_convert(rand(10000,99999), 10, 36);
        $main_dir = 'media/transactions/uploads';

        $data = [
            'filename' =>$upload->getClientOriginalName(),
            'extension'=> $upload->getClientOriginalExtension(),
            'entityID' => $transactionObj->id,
            'entityType' => 'Transaction',
            'folder'=> $main_dir,
            'fileRename' => $name.'_'.$transactionObj->id.'_'.$ran,
            'fileObject'=> $upload
        ];
        $uploaded_details = $this->documentRepo->uploads( $data);
        return $uploaded_details;
    }
    public function addUpload($document){
        $documentObj = $this->documentRepo->creation($document);
        $this->documentRepo->save($documentObj);
    }

}
