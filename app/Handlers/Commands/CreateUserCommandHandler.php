<?php namespace App\Handlers\Commands;
use App\Http\Requests\CreateNewUser;
use App\User;
use App\UserRole;
use Illuminate\Queue\InteractsWithQueue;
//use Sulsira\Uploader\Uploader;
use Realestate\User\UserRepository;
use Sulsira\Uploader\Uploader;

class CreateUserCommandHandler {

    protected $repository;
    public $user;
    public $role;
    function __construct(UserRepository  $repository, User $user, UserRole $role){
        $this->repository = $repository;
        $this->user = $user;
        $this->role = $role;
    }


    /**
     * @param CreateNewUser $command
     * @return bool
     */
    public function handle($command){

        $role =  (!empty($command->role))? $command->role : [];

        $user = $this->createuser($command);


        if( $role['profilePic'] ){

           $role = $this->upload_profile_pic($command, $user, $role);

        }

        $this->createrole($role, $user->id);

    }

    private function urlconstruct($domain){
        return strtolower($domain).'/dashboard';
    }

    private function createrole($role, $id){

        #there should be a method to prepare the data bcause this is a repository
        $role = array_add($role,'user_id',$id);
        $role = array_add($role,'url',$this->urlconstruct($role['domain']));
        $userrole = UserRole::creation($role);
        $this->repository->role( $userrole );
        return  $userrole;
    }
    private function createuser($command){
        $user = User::creation($command->user);
        $saved = $this->repository->save($user);
        return  $user ;
    }

    private function upload_profile_pic($command, $user, $role){

        // file pulling
        $main_dir = 'media/users/'.$command->role['usergroup'].'/'.$user->id;

        $img = Uploader::image($role['profilePic'])
            ->directory($main_dir)
            ->rename($user->id.'_'.'pp')
            ->resize(128,128, 'thumbsnails');
        $image = $img->details();

        $role = array_add( $role,'thumbnail', $image['new']['thumnail_location'].'/'.$image['new']['filename'] );
        $role = array_add($role,'image',  $image['new']['file'] );
        $role = array_add($role,'filename',$image['new']['filename'] );
        // $role = array_add($role,'url', $image['new']['folder']);

        return $role;
    }

}
