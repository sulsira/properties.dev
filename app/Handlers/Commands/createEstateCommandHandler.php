<?php namespace App\Handlers\Commands;

//use ;

use App\Estate;
use Illuminate\Queue\InteractsWithQueue;
use Realestate\Estate\EstateRepository;

class createEstateCommandHandler {
    public $estate;
    public $repository;

    /**
     * Create the command handler.
     *
     * @param Estate $estate
     * @param EstateRepository $repository
     * @return \App\Handlers\Commands\createEstateCommandHandler
     */
	public function __construct(Estate $estate, EstateRepository $repository)
	{
        $this->estate = $estate;
        $this->repository = $repository;
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle($command)
	{

		$estateObj = $this->estate->creation($command);

        $the_saved_estate = $this->repository->save($estateObj);

        return $the_saved_estate;

	}

}
