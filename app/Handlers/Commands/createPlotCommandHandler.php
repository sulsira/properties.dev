<?php namespace App\Handlers\Commands;

//use ;

use App\Plot;
use Illuminate\Queue\InteractsWithQueue;
use Realestate\Estate\EstateRepositoryInterface;
use Realestate\Plot\PlotRepository;

class CreatePlotCommandHandler {
    public $plot;
    public $repository;
    protected $estate;
    /**
     * Create the command handler.
     *
     * @param Plot $plot
     * @param PlotRepository $plotRepository
     * @param EstateRepositoryInterface $estate
     * @return \App\Handlers\Commands\CreatePlotCommandHandler
     */
	public function __construct(Plot $plot, PlotRepository $plotRepository, EstateRepositoryInterface $estate)
	{
		$this->plot = $plot;
        $this->repository = $plotRepository;
        $this->estate = $estate;
	}

	/**
	 * Handle the command.
	 *
     *
	 * @param    $command
	 * @return void
	 */
	public function handle( $command )
	{
		$plot_obj = $this->repository->creation($command);

        $estate = $this->estate->getById( $plot_obj->estate_id );

        $estate->plots()->save($plot_obj);

        $saved_plot =  $this->repository->save( $plot_obj );

        return $saved_plot;
	}

}
