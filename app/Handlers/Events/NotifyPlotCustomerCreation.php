<?php namespace App\Handlers\Events;

use App\Events\PlotCustomerWasCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class NotifyPlotCustomerCreation implements ShouldBeQueued{

    use InteractsWithQueue;
    /**
     * Create the event handler.
     *
     * @return \App\Handlers\Events\NotifyPlotCustomerCreation
     */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  PlotCustomerWasCreated  $event
	 * @return void
	 */
	public function handle(PlotCustomerWasCreated $event)
	{
        $this->release(30);
		dd($event);
        //now do what you want and show
        //implement the logic on to notify who and whenpp,
        //here is where we should call pusher to do the rest
	}

}
