<?php namespace App\Handlers\Events;

use App\Events\TransactionWasCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class NotifyTransactionCreation {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  TransactionWasCreated  $event
	 * @return void
	 */
	public function handle(TransactionWasCreated $event)
	{
		// calls the transaction

        // store in the notification table

        // send to pusher
	}

}
