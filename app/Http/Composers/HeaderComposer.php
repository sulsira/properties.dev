<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 6/26/2015
 * Time: 12:40 PM
 */

namespace App\Http\Composers;


use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Sulsira\Menu\Menu;

class HeaderComposer {
    public $target;
    public $navigation;
    public $request;
    public $user;

    /**
     * @param Menu $menu
     * @param Request $request
     * @param User $user
     */
    public function __construct(Menu $menu, Request $request, User $user){
        $this->navigation = $menu;
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * @param View $view
     */
    public function compose(View $view){
        $view->with('header', $this->headerComposer());
        $view->with('target', strtolower($this->request->path()));
        $view->with('user_details', $this->userSessions());
        $view->with('user_set', $this->userDetails());
    }

    public  function headerComposer(){
        return $this->navigation->topbar($this->userSessions());
    }

    public function userSessions(){

        $user_id = session('user_id');
        $security_level = session('security_level');
        $domain = session('domain');
        $usergroup = session('usergroup');
        $url  = session('url');
        $department = session('department');
        $previledges = session('previledges');

        return compact('user_id','security_level', 'domain','usergroup','url','department','previledges');
    }

    public function userDetails(){
       return $this->user->with('role','property')->where('id', '=', session('user_id') )->first();
    }
} 