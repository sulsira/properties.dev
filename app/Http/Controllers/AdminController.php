<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Sulsira\Menu\Menu;

class AdminController extends Controller {
use Traits\AdminTrait;
    /*
     * brings in all the dependancies
     * @params
     *
     * @return void
     *
     */

    public function __construct(){

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
//        $user_id = session('user_id');
//        $security_level = session('security_level');
//        $domain = session('domain');
//        $usergroup = session('usergroup');
//        $url  = session('url');
//        $department = session('department');
//
//        $arg = compact('user_id','security_level', 'domain','usergroup','url','department');
//        var_dump(session()->all());
//        dd("SOMETHIN IS NOT RIGHT");
		return view('admin.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
    /*
     * The direct dashboard view from the login action
     * @param void
     * @return view
     *
     */
    public function getDashboard(){
        return view('admin.profile');
    }

}
