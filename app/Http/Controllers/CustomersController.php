<?php namespace App\Http\Controllers;

use App\Commands\CreateCustomerCommand;
use App\Customer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateCustomerRequest;
use App\Variable;
use Realestate\Account\Bank\BankRepositoryInterface;
use Realestate\Account\Customer\CustomerRepositoryInterface;
use Realestate\Plot\PlotRepositoryInterface;
use \Request;

class CustomersController extends Controller {

    protected $customer;
    public function __construct(CustomerRepositoryInterface $customerRepository){
        $this->customer = $customerRepository;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('accounts/customers/index', ['data'=> $this->customer->tabulardata()]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @param PlotRepositoryInterface $plots
     * @param BankRepositoryInterface $bank
     * @return Response
     */
	public function create(PlotRepositoryInterface $plots, BankRepositoryInterface $bank)
	{
        $plots = $plots->available();
        $banks = ($bank->getAll())? $bank->getAll()->lists('name','id') : [];
        $countries = Variable::domain('country')->lists('Vari_VariableName','Vari_VariableName');
		if( Request::ajax()){
            $input = \Input::all();
            $modalID = $input['id'];
            $route = 'accounts.customers.create';
            return \Response::json(view('accounts/customers/createModal',compact('modalID','route', 'plots', 'countries','banks'))->render());
        }
		return view('accounts/customers/create',compact('plots', 'countries','banks'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCustomerRequest $request
     * @return Response
     */
	public function store(CreateCustomerRequest $request)
	{
//        dd(\Input::all());

        //
        $this->dispatchFrom( CreateCustomerCommand::class, $request );

        flash()->success('You have created a customer', 'Nice work !');

        return redirect()->to('accounts/customers');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
