<?php namespace App\Http\Controllers;

use App\Commands\createEstateCommand;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateEstateRequest;
use App\User;
use Illuminate\Support\Facades\Input;
use Realestate\Estate\EstateRepositoryInterface;
use \Request;

class EstatesController extends Controller {

    /*
     * @var
     *
     * */
    public $estate;
    /**
     * @param EstateRepositoryInterface $estate
     */
    public function __construct(EstateRepositoryInterface $estate){
        $this->estate = $estate;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// all these thigns sould go in the store method

       $all = $this->estate->getAll();
        dd($all);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function create()
	{
        if( Request::ajax()){
            $input = \Input::all();
            $modalID = $input['id'];
            $route = 'estates.create';
            return \Response::json(view('estates.modalCreate',compact('modalID','route'))->render());
        }
        return view('estates.create') ;
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateEstateRequest $request
     * @return Response
     */
	public function store(CreateEstateRequest $request)
	{

        $this->dispatchFrom(createEstateCommand::class,$request);

        flash()->overlay("You have succesfully create an Estate", "Good Job");

        return redirect()->to('estates');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
