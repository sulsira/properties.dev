<?php namespace App\Http\Controllers;

use App\Commands\CreateExpenseCommand;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateExpenseRequest;
use Illuminate\Http\Request;

class ExpensesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('accounts/expenses/create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateExpenseCommand|CreateIncomeRequest|CreateExpenseRequest $request
     * @return Response
     */
	public function store(CreateExpenseRequest $request)
	{
        $this->dispatchFrom(CreateExpenseCommand::class, $request);

        flash()->success('you have successfully added an income');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
