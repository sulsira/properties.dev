<?php namespace App\Http\Controllers;

use App\Commands\CreateInvoiceCommand;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateInvoiceRequest;
use App\Variable;
use Illuminate\Http\Request;
use Realestate\Account\Customer\CustomerRepositoryInterface;
use Realestate\Plot\PlotRepositoryInterface;

class InvoicesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

    /**
     * Show the form for creating a new resource.
     *
     * @param PlotRepositoryInterface $plots
     * @param CustomerRepositoryInterface $customers
     * @return Response
     */
	public function create(PlotRepositoryInterface $plots, CustomerRepositoryInterface $customers)
	{


        $customers = list_customers_with_entities($customers->customersWithEntity());
		$plots = $plots->available();
        $countries = Variable::domain('country')->lists('Vari_VariableName','Vari_VariableName');
		return view('accounts/invoices/create',compact('plots','customers'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateInvoiceRequest $request
     * @return Response
     */
	public function store(CreateInvoiceRequest $request)
	{
		$this->dispatchFrom(CreateInvoiceCommand::class, $request);


        flash()->overlay("You have succesfully create an Invoice", "Good Job");

        return redirect()->to('invoices');
	}

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
	public function show( $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
