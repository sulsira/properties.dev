<?php namespace App\Http\Controllers;

use App\Commands\createPlotCommand;
use App\Estate;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreatePlotRequest;
use App\Plot;
use \Request;

class PlotsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

    /**
     * Show the form for creating a new resource.
     *
     * @param Estate $estate
     * @return Response
     */
	public function create(Estate $estate)
	{

         $estates = $estate->all()->lists('name', 'id');
         $customers = ['somethong one', 'something ', 'elses'];
         $agents = ['somethong one', 'something ', 'elses'];
         $estate_id  = '';
		 $plot_customer_id = '';
		 $agent_id = '';
		 $purchaseType = ['Mortgage'=>'Mortgage', 'sold'=>'sold', 'lease'=>'lease', 'rent'=>'rent', 'others'=>'others'];
		 $availability = ['available'=> 'available', 'un-available'=> 'un-available', 'sold'=> 'sold' ,'others'=> 'others'];

         if( Request::ajax()){

             $input = \Input::all();
             $modalID = $input['id'];
             $route = 'plots.create';
             return \Response::json(view('plots.modalCreate',compact('modalID','route','plot_id','customers','plot_customer_id','agent_id','agents','availability','purchaseType','estates', 'estate_id'))->render());
         }

		 return view('plots.create', compact('estates','estate_id','customers','plot_customer_id','agent_id','agents','availability','purchaseType'));


	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePlotRequest $request
     * @return Response
     */
	public function store(CreatePlotRequest $request)
	{
       $this->dispatchFrom(CreatePlotCommand::class, $request);

       flash()->overlay('You have created a plot', 'Nice work !');

       return redirect()->to('plots/create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
