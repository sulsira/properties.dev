<?php namespace App\Http\Controllers;

use App\Commands\CreatePlotCustomerCommand;
use App\Estate;
use App\Events\PlotCustomerWasCreated;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreatePlotCustomerRequest;
use App\Variable;
use Realestate\Plot\Customer\CustomerRepositoryInterface;
use Realestate\Plot\PlotRepositoryInterface;
use \Request;

class PlotsCustomersController extends Controller {
    /**
     * @var
     */
    protected $plotcustomers;

    public function __construct(CustomerRepositoryInterface $plotcustomers){
        $this->plotcustomers = $plotcustomers;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('plots.customers.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @param PlotRepositoryInterface $plots
     * @return Response
     */
	public function create(PlotRepositoryInterface $plots)
	{
        $plots = $plots->available();
        $countries = Variable::domain('country')->lists('Vari_VariableName','Vari_VariableName');

        if( Request::ajax()){
            $input = \Input::all();
            $modalID = $input['id'];
            $route = 'plots.customers.create';
            return \Response::json( view('plots.customers/modalCreate',compact('modalID','route','plots','countries') )->render() );
        }

        return view('plots.customers.create', compact('estates','plots','countries'));

	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePlotCustomerRequest $request
     * @return Response
     */
	public function store(CreatePlotCustomerRequest $request)
	{
        $this->dispatchFrom( CreatePlotCustomerCommand::class, $request );

        flash()->success('You have created a plot customer', 'Nice work !');

        return redirect()->to('plots/customers');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
