<?php namespace App\Http\Controllers;

use App\Commands\CreateReceiptCommand;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateReceiptRequest;
use Illuminate\Http\Request;

class ReceiptsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('accounts/receipts/create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateReceiptRequest $request
     * @return Response
     */
	public function store(CreateReceiptRequest $request)
	{

       $this->dispatchFrom(CreateReceiptCommand::class, $request);


        flash()->overlay("You have succesfully create a receipt", "Good Job");

        return redirect()->to('receipts');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
