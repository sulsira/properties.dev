<?php namespace App\Http\Controllers;

use App\Commands\CreateTransactionCommand;
use App\Customer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateTransactionRequest;
use App\Plot;
use App\PlotsCustomer;
use App\PlotsPayment;
use Illuminate\Http\Request;
use Realestate\Account\Bank\BankRepositoryInterface;
use Realestate\Account\Customer\CustomerRepositoryInterface;
use Realestate\Plot\PlotRepositoryInterface;

class TransactionsController extends Controller {


    public function __construct(){

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

    /**
     * Show the form for creating a new resource.
     *
     * @param BankRepositoryInterface $bank
     * @param CustomerRepositoryInterface $customer
     * @param PlotRepositoryInterface $plots
     * @return Response
     */
	public function create(BankRepositoryInterface $bank, CustomerRepositoryInterface $customer, PlotRepositoryInterface $plots)
	{

        $banks = ($bank->getAll())? $bank->getAll()->lists('name','id') : [];
        $customer = $customer->listByDepartment();
        $plots = $plots->available();


        $customers = [];
        foreach($customer as $key => $value){

                foreach($value as $ind=>$sets){
                    $customers[$key][$sets['id']] = $sets['name'];



                }
//
        }

//        dd(array_flatten($customers));
//           $pc =  Customer::find(1);
//           dd($pc->customable()->with('plots')->get()->toArray());
//        extract($customers);
//        $plot = Customer::find(1)->customable->toArray();
//        dd($plot);
		return view('accounts/transactions/create',compact('banks','customers','plots'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTransactionRequest $request
     * @return Response
     */
	public function store(CreateTransactionRequest $request)
	{

        $this->dispatchFrom( CreateTransactionCommand::class, $request);

        flash()->overlay('You have created a transaction', 'Go On !');

        return redirect()->to('accounts/transactions');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
