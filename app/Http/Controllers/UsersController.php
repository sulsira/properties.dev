<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateNewUser;
use Illuminate\Http\Request;
use App\Commands\CreateUserCommand;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller {
    use Traits\AdminTrait;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('users.index');
//        $request = array_add(['something'=>'thing','else'=>'elsingingking'], $request);
//        $this->dispatch(new newusercommand( $input ) );
//        $this->dispatch(newusercommand(), $input );
//        $request = \Illuminate\Http\Request::create('/','get', ['else'=>'something else']);
//        $this->dispatchFrom(CreateUserCommand::class,  $request);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @args CreateNewUser  object, comandbus bus, commandhandler hanlde
     * @param CreateNewUser $request
     * @internal param $
     * @return Response
     */
	public function store(CreateNewUser $request)
	{

//        task

//        also sucess messages showing the user have been created
//        send an email to the user using my server
        // at the end check autorization
        // build a small call for creating veda and it should return true or false

        $this->dispatch(new CreateUserCommand($request) );
        return Redirect::back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
