<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller {
use Traits\PublicTrait;
	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @sets public value
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}



	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('public/login');
		// var_dump($this->menu());
	}

}
