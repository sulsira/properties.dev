<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\UserRole;
use Realestate\User\UserRepository;

class CreateEstateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param UserRepository $useris
     * @internal param UserRepository $userRepository
     * @internal param UserRole $userrole
     * @return bool
     */
	public function authorize(UserRepository $useris)
	{
		return $useris->authorizeTo()->add();
	}

	/**0
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|unique:estates|max:200',
            'location' => 'max:200'
		];
	}

    public function forbiddenResponse(){

        return redirect()->to('rejected');
    }
}
