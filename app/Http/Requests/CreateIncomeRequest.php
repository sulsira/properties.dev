<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Realestate\User\UserRepository;

class CreateIncomeRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param UserRepository $useris
     * @return bool
     */
	public function authorize(UserRepository $useris)
	{
        return $useris->authorizeTo()->add();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

}
