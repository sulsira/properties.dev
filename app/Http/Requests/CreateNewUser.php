<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateNewUser extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        // check the person secutiry level
        // check the persons priviledges
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'email' => 'required|email|unique:users|max:200',
            'password'=> 'required|max:200',
            'security_level' => 'required|numeric|max:200',
            'first_name' => 'max:200',
            'last_name' => 'max:200',
            'department' => 'max:200',
            'phone' => 'max:200',
            'profilePic' => 'image'
        ];
	}

}
