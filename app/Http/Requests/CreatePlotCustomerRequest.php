<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\UserRole;
use Realestate\User\UserRepository;
class CreatePlotCustomerRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param UserRepository $useris
     * @return bool
     */
	public function authorize(UserRepository $useris)
	{
        return $useris->authorizeTo()->add();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
              "pers_fname" => "required|max:200",
              "pers_mname" => "max:200",
              "pers_lname" => "required|max:200",
              "day" => "numeric",
              "month" => "numeric",
              "year" => "numeric",
              "pers_gender" => "max:200",
              "pers_nationality" => "max:200",
              "contact.phones" => 'max:200',
              "contact.emails" => 'max:200',
              "contact.address" => 'max:200',
              "customerType" => "in:mortgage,fullpayment",
              "paymentPeriodInMonths" => "numeric|max:200",
              "paymentType" => "in:mortgage,fullpayment",
              "pers_indentifier" => "max:200",
              "fullname" => "max:200",
              "contacts" => "max:200",
              "photo" => "max:5000|mimes:jpeg,png,jpg",
              "document" =>"max:5000|mimes:doc,docx,pdf"
		];
	}
    public function forbiddenResponse(){
        return redirect()->to('rejected');
    }
}
