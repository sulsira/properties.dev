<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Realestate\User\UserRepository;

class CreatePlotRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param UserRepository $useris
     * @return bool
     */
	public function authorize(UserRepository $useris)
	{
        return $useris->authorizeTo()->add();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
              'estate_id' => '',
              'plot_customer_id' => '',
              'agent_id' => '',
              'indentification' => 'max:50',
              'size' => 'max:200',
              'price' => 'max:200',
              'availability' => 'in:available,un-available,sold,others',
              'purchaseType' => 'in:Mortgage,sold,lease,rent,others',
              'remarks' => 'max:250'
		];
	}

}
