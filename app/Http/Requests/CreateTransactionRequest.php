<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Realestate\User\UserRepository;

class CreateTransactionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param UserRepository $useris
     * @return bool
     */
	public function authorize(UserRepository $useris)
	{
		return $useris->authorizeTo()->add();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            "title" => "",
            "description" => "",
            "revenue" => "",
            "account" => "",
            "receipt_number" => "",
            "cheque_reference" => "",
            "bank_number" => "",
            "bank_account" => "",
            "category" => "",
            "customer" => "",
            "total" => "",
            "for" => ""

		];
	}

//    public function forbiddenResponse(){
//
//        return redirect()->to('rejected');
//    }
}
