<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public  function forbiddenResponse(){

        return redirect()->to('rejected');
    }

}
