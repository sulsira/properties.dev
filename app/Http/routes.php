<?php
// use Realestate/Traits/Admin;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|colo
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// $menu->sessions($data/userdate) / MENU::input($menudata = [])->sessions(data)
			// ->filterby([index]=> asc, desc,[except]=>['dashboard'] = null)
			// ->domain([view=>all, self, specific] = null)
			// ->securityLevel(['highest#'=>'1',lowest=>'8']= null)
			// ->get([all,visible,hidden, ] = null);

// menu = $routes->middleware('checker')

// $r = new  Sulsira\Accounts\Rent;
//dd($r->payment([
//    'payment' => 400,
//    'month_fee' => 100,
//    'balance'=> 300,
//    'last_paid_date' => '2015-1-31',
//    'current_payment_date' => date("Y-m-d", time()),
//    'previouly_assigned_payment_date'=> '2015-4-30'
//
//]));
//;
//
//dd( Accounts::mortgage()->pay([
//       'amount_paid' => 500 + 50+0,
//       'total_paid' => 0,
//       'payment_due'=> 0,
//       'payment_duration_in_months'=> 30,
//       'calculated_monthly_fee' => 0,
//       'number_of_months_paid' => 0,
//       'unhindered_closing_payment_date'=> null,
//       'next_payment_date' => null,
//       'commencement_date' => '2015-7-26',
//       'payment_type' => null,
//       'down_payment' => 5000,
//       'plot_price' => 75000,
//       'paid_date' => date("Y-m-d", time()),
//       'arrears' => null,
//       'balance' => null
//   ])->getPayment()
//);

//$account = new Sulsira\Accounts\Account::rent();
//var_dump($account->rent());
//// ACCOUNTS
//
//        ->RENT/MORTGATE/LOTSALE/BANCK/EXPENSES/CHARGES/SALARY/()
//          ->PAY/GET/DELETE/()
//        ->getTrasactions/get/getStatus/
//dd(
//   Sulsira\Accounts\Mortgage::pay()->getPayment()
//);

//use Illuminate\Support\Facades\Session;use Sulsira\Menu\Menu;

//$user_id = session('user_id');
//$security_level = session('security_level');
//$domain = session('domain');
//$usergroup = session('usergroup');
//$url  = session('url');
//$department = session('department');
//
//$arg = compact('user_id','security_level', 'domain','usergroup','url','department');
//var_dump($arg);
//var_dump(Sulsira\Menu\Menu::topbar());
// date('birthday', unix, );

Route::get('/','WelcomeController@index');

Route::get('logout', 'Auth\AuthController@getLogout');
Route::get('home', 'HomeController@index');
Route::get('rejected', 'PublicController@getRejected');
Route::post('login', 'Auth\AuthController@postLogin');
Route::controllers([
	'password' => 'Auth\PasswordController'
]);

// admin based routes
Route::resource('admin/dashboard', 'AdminController@getDashboard');
Route::resource('admin', 'AdminController');
Route::resource('users', 'UsersController');

// estate based routes
Route::get('estate/dashboard', 'EstateController@getDashboard');
Route::resource('estate', 'EstateController');
Route::resource('estates', 'EstatesController');

Route::group(['prefix'=>'plots'], function(){
//    Route::get('plots/customers', 'PlotsCustomersController@index');
    Route::resource('customers', 'PlotsCustomersController');
});
Route::resource('plots', 'plotsController');

// accounts based routes
Route::group(['prefix'=>'accounts'], function(){

    Route::resource('dashboard', 'AccountsController@getDashboard');

    //account customers base routes
    Route::resource('customers', 'CustomersController');
    Route::resource('transactions', 'TransactionsController');
    Route::resource('invoices', 'InvoicesController');
    Route::resource('receipts', 'ReceiptsController');
    Route::resource('incomes','IncomesController');
    Route::resource('expenses','ExpensesController');

    Route::resource('/', 'AccountsController');
});
//
//DB::listen(function($q, $b, $t){
//   var_dump($q);
//});
