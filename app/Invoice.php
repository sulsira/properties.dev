<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {

	protected $table = 'invoices';
    protected $primaryKey = 'id';

    protected $fillable = [
        "productService",
        "description",
        "quantity",
        "amount",
        "dueDate",
        "issuedDate"
    ];


    public function setDuedateAttribute($date){
        $this->attributes['dueDate'] = Carbon::parse($date);
    }
    public function setIssueddateAttribute($date){
        $this->attributes['issuedDate'] = Carbon::parse($date);
    }
}
