<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NextKin extends Model {

    protected $table = 'next_kins';
    protected $primaryKey = 'id';
    protected $fillable = ['EntityID', 'EntityType', 'fullname', 'contacts', 'address'];
}
