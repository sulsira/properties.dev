<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model {
    protected $table = 'images';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'name', 'entity_type', 'entity_ID', 'type', 'fullpath', 'filename', 'foldername', 'extension', 'filetype', 'thumnaildir','image'];
	//

}
