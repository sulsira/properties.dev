<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Plot extends Model {
    protected  $table = 'plots';
    protected $primaryKey = 'id';
    protected $fillable = ['estate_id', 'agent_id', 'plot_customer_id', 'name', 'indentification', 'size' ,'price', 'status', 'availability', 'total_payment', 'purchaseType', 'remarks'];

    public static function creation($data){

        $data = (array) $data;

        $entity = new static($data);

        return  $entity;

    }
    // relationships
    public function estate(){
       return $this->belongsTo('App\Estate');
    }
    public function customer(){
        return $this->belongsTo('App\PlotsCustomer','plot_customer_id','id');
    }
    public function agents(){
//        $this->hasMany('App\Agent');
    }
    public function payments(){
       return $this->hasMany('App\PlotsPayment','plot_id', 'id');
    }
    public function transactions(){
        return $this->hasMany('App\Transaction','plot_id', 'id');
    }
    // scopes


    /**
     * returns all plots that avalable
     * @param $query
     */
    public function scopeAvailable($query){
        return $query->whereRaw('status = ? ',[1])->get(); // one means available 0 mean not
    }
    public function scopeOwned($query){
        return $query->whereRaw('plot_customer_id !=',[0])->get();
    }
    public function scopeUnAvailable($query){
        // returns all plots that unavalable
        return $query->whereRaw('availability = ? AND plot_customer_id !=',['un-available', 0])->get();
    }
    public function scopeSold($query){
        // returns all plots that are sold and there customer
    }
    public function scopeMortgage($query){
        // returns all plots that mortgage
    }
    public function scopeCompletedPayment($query){
        // returns all plots that avalable
    }
    public function scopeActivelyPaying($query){
        // returns all plots that avalable
    }
    public function scopeInactivePaying($query){
        // returns all plots that avalable
    }
    public function scopePaymentStatus($query, $plotID = '*'){
        // gourps all the plots into payment groups like complete in a group, ongonig and so on
    }
    public function scopeAllPayments($query, $plotID = '*'){
        // the payments made on a plot and group by date by month
    }
    public function scopeAllCustomers($query, $plotId = '*'){
        // returns a customer if only one is involve is many return all with there payments
    }
    public function scopeDelete(){
        // find the deleted plots by status
    }

    // misc
}
