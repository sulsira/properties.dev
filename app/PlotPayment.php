<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PlotPayment extends Model {

	protected $table = 'plots_payments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'plot_id', 'user_id', 'agent_id', 'plot_customer_id', 'paymentType', 'transaction', 'paymentStatus', 'downPayment', 'totalPayment', 'paymentPeriodInMonths', 'monthlyFee', 'balance', 'arrears', 'paymentCommencementDate','normalClosePaymentDate','dateOfPayment'
    ];

}
