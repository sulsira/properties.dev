<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PlotsCustomer extends Model {

	protected $table = 'plots_customers';
    protected $primaryKey = 'id';
    protected $fillable = ['plot_id', 'partner_id', 'person_id', 'payment_id', 'customerType', 'status'];

    // relationships
//
    public function person(){
        return $this->hasOne('App\Person','id','person_id');
    }

    public function plots(){
        return $this->hasMany('App\Plot','plot_customer_id','id');
    }

    public function customers(){
        return $this->morphMany('App\Customer','customable');
    }

    public function payments(){
        return $this->hasMany('App\PlotsPayment', 'plot_customer_id', 'id');
    }
}
