<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PlotsMortgateTransaction extends Model {

	protected $table = 'plots_mortgage_transactions';
    protected $primaryKey = 'id';
    protected $fillable = ['plot_id', 'user_id', 'plot_payment_id', 'plot_customer_id', 'amount_paid', 'datePaidFor', 'monthPaidFor', 'yearOfPayment', 'deleted'];

}
