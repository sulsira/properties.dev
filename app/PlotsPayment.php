<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PlotsPayment extends Model {
    protected $table = 'plots_payments';
    protected $primaryKey = 'id';
protected  $fillable = ['plot_id', 'user_id', 'agent_id', 'plot_customer_id', 'paymentType', 'transaction', 'paymentStatus', 'downPayment', 'duePayment', 'dateOfPayment', 'totalPayment', 'paymentPeriodInMonths', 'monthlyFee', 'balance', 'arrears', 'normalClosePaymentDate', 'paymentCommencementDate'
];

    public function customer(){
        return $this->belongsTo('App\PlotsCustomer', 'plot_customer_id', 'id');
    }

    public function plot(){
        return $this->belongsTo('App\Plot', 'plot_id', 'id');
    }
}
