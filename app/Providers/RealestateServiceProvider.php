<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RealestateServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{

        $this->app->bind('Realestate\Estate\EstateRepositoryInterface', 'Realestate\Estate\EstateRepository');
        $this->app->bind('Realestate\Plot\Customer\CustomerRepositoryInterface', 'Realestate\Plot\Customer\CustomerRepository');
        $this->app->bind('Realestate\Person\PersonRepositoryInterface', 'Realestate\Person\PersonRepository');
        $this->app->bind('Realestate\Plot\Payment\PaymentRepositoryInterface', 'Realestate\Plot\Payment\PaymentRepository');
        $this->app->bind('Realestate\Plot\Kin\NextKinRepositoryInterface', 'Realestate\Plot\Kin\NextKinRepository');
        $this->app->bind('Realestate\Person\Contact\ContactRepositoryInterface', 'Realestate\Person\Contact\ContactRepository');
        $this->app->bind('Realestate\Person\Picture\PictureRepositoryInterface', 'Realestate\Person\Picture\PictureRepository');
        $this->app->bind('Realestate\Document\DocumentRepositoryInterface', 'Realestate\Document\DocumentRepository');
        $this->app->bind('Realestate\Plot\PlotRepositoryInterface', 'Realestate\Plot\PlotRepository');
        $this->app->bind('Realestate\Account\Bank\BankRepositoryInterface', 'Realestate\Account\Bank\BankRepository');
        $this->app->bind('Realestate\Account\Customer\CustomerRepositoryInterface', 'Realestate\Account\Customer\CustomerRepository');
        $this->app->bind('Realestate\Account\Transaction\TransactionRepositoryInterface', 'Realestate\Account\Transaction\TransactionRepository');
        $this->app->bind('Realestate\Account\Invoice\InvoiceRepositoryInterface', 'Realestate\Account\Invoice\InvoiceRepository');
        $this->app->bind('Realestate\Account\Receipt\ReceiptRepositoryInterface', 'Realestate\Account\Receipt\ReceiptRepository');
        $this->app->bind('Realestate\Account\Income\IncomeRepositoryInterface', 'Realestate\Account\Income\IncomeRepository');
        $this->app->bind('Realestate\Account\Expense\ExpenseRepositoryInterface', 'Realestate\Account\Expense\ExpenseRepository');
	}

}
