<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->composerNavigationBar();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    public function composerNavigationBar(){
        // believe something else better can be done here like not bloat all the view compwers in one  method
        view()->composer('templates.admin', 'App\Http\Composers\HeaderComposer');
        view()->composer('templates.estate', 'App\Http\Composers\HeaderComposer');
        view()->composer('templates.account', 'App\Http\Composers\HeaderComposer');

    }

}
