<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 8/1/2015
 * Time: 1:32 AM
 */

namespace Realestate\Account\Bank;


use App\Bank;
use Realestate\DbRepository;

class BankRepository extends DbRepository implements BankRepositoryInterface {

    protected  $model;

    public function __construct(Bank $bank){
        $this->model = $bank;
    }
    public function save(Bank $bank){
        return $bank->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->find($id);
    }

    public function getAll(){
        return $this->model->all();
    }
}