<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 8/1/2015
 * Time: 1:32 AM
 */
namespace Realestate\Account\Bank;


use App\Bank;

interface BankRepositoryInterface {

    public function save( Bank $bank );

    public function getAll();

    public function getById($id);
} 