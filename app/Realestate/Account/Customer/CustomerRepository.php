<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/30/2015
 * Time: 5:00 AM
 */

namespace Realestate\Account\Customer;


use App\Customer;
use App\PlotsCustomer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;
use Realestate\DbRepository;
use Realestate\Person\Contact\ContactRepositoryInterface;

class CustomerRepository extends DbRepository implements CustomerRepositoryInterface{
    protected  $model;

    public function __construct(Customer $customer){
        $this->model = $customer;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function save(Customer $customer){
        return $customer->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->whereId($id)->get();
    }

    public function getAll(){
        return $this->model->all();
    }
    public function creation($data)
    {
        // TODO: Implement creation() method.
    }
    public function listByDepartment($feilds = null){
        $sets = [];
        $customers = $this->getAll()->toArray();
        if(!empty($customers)){
            foreach($customers as $ind => $customer){
                $id = (int)$customer['id'];
                $domain = preg_split("/\\\\/",$customer['customable_type'])[1];
                $model = App::make($customer['customable_type']);
                $prep = [];
                foreach($model->whereId($id)->with(['person'])->get() as $colect){
                    $prep = ['id'=> $id , 'name'=>$colect->person['pers_fname'].' '.$colect->person['pers_mname'].' '.$colect->person['pers_lname']];
                }
                $sets[ $domain ][] = $prep;
            }

        }

        return $sets;
    }
    public function customersWithEntity($entities_attr = []){
        $data = [];
        // returns a list of customers with enities
       foreach($this->listByDepartment() as $mod => $customer){

           if($mod == 'PlotsCustomer'){

               foreach($customer as $key => $details){

                   $set =  $this->model->find($details['id']);
                   $done = $set->customable()->with('plots')->first()->toArray();

                   $plots = [];


                   foreach($done['plots'] as $plot){


                       if(!empty($plot)){


                           $plots[$plot['id']] = ($plot['name'])?: $plot['indentification'];

                       }else{

                           $plots[] = null;

                       }

                   }
                   $data[$mod][] = ['customer_id'=>$details['id'], 'fullname'=> $details['name'],'plots' => $plots ];

               }

           }



       }

        return $data;
    }

    /**
     * this would bring together all customers and group then for the data tables
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function tabulardata(){

        $PlotsCustomer = $this->getPlotcustomers()?: [];

        return compact('PlotsCustomer');

    }
    private function getPlotcustomers(){
        return PlotsCustomer::whereStatus(1)->with(['person.contacts', 'customers','payments'=>function($query){
            return $query->groupBy('transaction');
        }])->get();
    }


}