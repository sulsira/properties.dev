<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/30/2015
 * Time: 5:01 AM
 */

namespace Realestate\Account\Customer;


use App\Customer;

interface CustomerRepositoryInterface {
    public function save(Customer $customer);
    public function getById( $id );
    public function getAll();
    public function creation($data);
    public function listByDepartment($feilds = null);
    public function customersWithEntity();
    public function tabulardata();
} 