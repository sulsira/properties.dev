<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 9/8/2015
 * Time: 4:25 PM
 */

namespace Realestate\Account\Expense;


use App\Expense;
use Realestate\DbRepository;

class ExpenseRepository extends DbRepository implements ExpenseRepositoryInterface{
    protected  $model;

    public function __construct(Expense $expense){
        $this->model = $expense;
    }
    public function save(Expense $expense){
        return $expense->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->find($id);
    }

    public function getAll(){
        return $this->model->all();
    }
    public function creation($data)
    {
        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;
    }
} 