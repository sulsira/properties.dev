<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 9/8/2015
 * Time: 4:26 PM
 */

namespace Realestate\Account\Expense;


use App\Expense;

interface ExpenseRepositoryInterface {
    public function save(Expense $expense);

    public function getById( $id );

    public function getAll();

    public function creation($data);
} 