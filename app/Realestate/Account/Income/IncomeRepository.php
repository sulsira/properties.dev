<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 9/8/2015
 * Time: 3:24 PM
 */

namespace Realestate\Account\Income;


use App\Income;
use Realestate\DbRepository;

class IncomeRepository extends DbRepository implements IncomeRepositoryInterface{
    protected  $model;

    public function __construct(Income $income){
        $this->model = $income;
    }
    public function save(Income $income){
        return $income->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->find($id);
    }

    public function getAll(){
        return $this->model->all();
    }
    public function creation($data)
    {
        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;
    }

} 