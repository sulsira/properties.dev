<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 9/8/2015
 * Time: 3:24 PM
 */

namespace Realestate\Account\Income;


use App\Income;

interface IncomeRepositoryInterface {
    public function save(Income $income);

    public function getById( $id );

    public function getAll();

    public function creation($data);
} 