<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/30/2015
 * Time: 5:04 AM
 */

namespace Realestate\Account\Invoice;


use App\Invoice;
use Realestate\DbRepository;

class InvoiceRepository extends DbRepository implements InvoiceRepositoryInterface {
    protected  $model;

    public function __construct(Invoice $invoice){
        $this->model = $invoice;
    }
    public function save(Invoice $invoice){
        return $invoice->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->find($id);
    }

    public function getAll(){
        return $this->model->all();
    }
    public function creation($data)
    {
        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;
    }

} 