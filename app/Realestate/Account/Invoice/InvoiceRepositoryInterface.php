<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/30/2015
 * Time: 5:04 AM
 */

namespace Realestate\Account\Invoice;


use App\Invoice;

interface InvoiceRepositoryInterface {

    public function save(Invoice $invoice);

    public function getById( $id );

    public function getAll();

    public function creation($data);
} 