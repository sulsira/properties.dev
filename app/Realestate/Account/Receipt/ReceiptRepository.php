<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 9/7/2015
 * Time: 11:36 PM
 */

namespace Realestate\Account\Receipt;


use App\Receipt;
use Realestate\DbRepository;

class ReceiptRepository extends DbRepository implements ReceiptRepositoryInterface {
    protected  $model;

    public function __construct(Receipt $receipt){
        $this->model = $receipt;
    }

    /**
     * @param Receipt $receipt
     * @return bool
     */
    public function save(Receipt $receipt){
        return $receipt->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->find($id);
    }

    public function getAll(){
        return $this->model->all();
    }
    public function creation($data)
    {
        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;
    }

} 