<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 9/7/2015
 * Time: 11:36 PM
 */

namespace Realestate\Account\Receipt;


use App\Receipt;

interface ReceiptRepositoryInterface {

    public function save(Receipt $receipt);

    public function getById( $id );

    public function getAll();

    public function creation($data);
} 