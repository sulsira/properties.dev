<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 8/1/2015
 * Time: 11:35 PM
 */

namespace Realestate\Account\Transaction;


use App\Customer;
use App\PlotsCustomer;
use App\Transaction;
use Realestate\Account\Customer\CustomerRepository;
use Realestate\DbRepository;
use Realestate\Plot\Customer\CustomerRepositoryInterface;
use Sulsira\Accounts\Account;

class TransactionRepository extends DbRepository implements TransactionRepositoryInterface {
    protected $model;
    protected $account;
    function __construct(Transaction $transaction, Account $account, CustomerRepository $customer){
        $this->model = $transaction;
        $this->account = $account;
        $this->customer = $customer;
    }

    public function save(Transaction $transaction)
    {
        return $transaction->save();
    }


    public function creation($data){



        $data = $this->prepare($data);

        extract($data);

        if(is_array($data['paymentMedium'])){

            $paymentMethod = array_pull($data, 'paymentMedium');

            $paymentMedium = array_pull($paymentMethod, 'account');

            if($paymentMethod){

                foreach($paymentMethod as $key => $value){

                    $data = array_add($data, $key, $value );

                }
            }
            $data = array_add($data, 'paymentMedium', $paymentMedium );
        }




        if(empty($data)) return [];

        $entity = $this->model->fill($data);

        return  $entity;

    }

    private function prepare($data){
       $cat_data = $this->category($data);

        return  $cat_data;

    }

    private function transactions($data){
//        takes the data put it in an array where the keys are modals or database tables where the data should go
    }

    private function category($data){
        $cat_set = ['others','mortgage', 'fullpayment', 'rent'];
        $issue = (isset($data->data['category'])) ? $data->data  : $data ;

        $category = (isset($issue['data']))? (string)strtolower($issue['data']['category']) : $issue['category'];

        $catch = false;
        // category would help i knowing where the payment is directed to but what happens when there is no category
        if( ! $category ){
            return $this->nocat($data);
        }
        foreach($cat_set as $key => $value){
            if($category == $value){
                $catch  = true;
            }
        }
        return ($catch)? $this->$category($issue) : [];
    }

    private function nocat($data){
        var_dump("Error: cannot process transaction");

    }

    private function rent($input_data){
        $flag = [];
        extract( $this->confirmCustomer($input_data) );

//        dd($this->confirmCustomer($input_data));

        if($customer !== null and $customer_type !== 'RentCustomer'){
            $customer = null;
            $data['customer_id'] = 0;
            $flag['error'] = 'wrong customer';
            $flag['massage'] = 'Customer was '.$customer_type;
            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');

        }
//        var_dump($data['paymentDate']);
////        $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true,'-');
//        dd($data['paymentDate']);

        // then go get his payment details

        // including his house


        // then prepare an array or key pair values to use the rent payer accountant (you would need DTO to match)


        // rent accountant calculated if this is a first payment or continues and if there are arrear and
//        if there are balance balances and explains


        return compact('flag', 'customer', 'data', 'customer_type' );
    }
    private function rentDto($data){
        extract($data);
        return [
            'payment' => $amountPaid,
            'month_fee' => 0,
            'balance'=> 0,
            'last_paid_date' => null,
            'current_payment_date' => $paymentDate,
            'previouly_assigned_payment_date'=> null
        ];
    }

    /**
     * @param $data
     */
    private function mortgage($data){
        $flag = [];
        extract( $this->confirmCustomer($data) );

        if($customer_type == 'PlotsCustomer'){

            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');

        }elseif($customer !== null and $customer_type !== 'PlotsCustomer') {

            $customer = null;
            $data['customer_id'] = 0;
            $flag['warning'] = 'no plot select';
            $flag['massage'] = 'Confirm and select plot';
            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');

        }

        if(isset($data['customer_id']) and $data['customer_id'] !== 0){

         $customer_plots_payments =  $customer->with('plots','payments')->first()->toArray();
         extract($customer_plots_payments);


            if( (empty($payments) or count($payments) != 1) or count($plots) !=  1 ){

//                $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');



            }elseif(count($plots) ==  1 and count($payments) == 1){

                // we can pay the plot and send a confirmation
                $cds = $this->mortgageDTO($customer_plots_payments, $data);
                // we have to do is to put the plot_id in transaction and do the payment after
            }


           // get the customer relationships: plots->payments->lastpayment();

            // calculate the payment using the accounts package

            // add that to an array with key of model

            return compact('data','customer','customer_type', 'flag');

        }

//       $customers =  (object)$customer;
//
//        $help = PlotsCustomer::find($customers->id)->with('plots')->get()->toArray();
//        var_dump($help);
//        $customerPlot = $customer->with('plots')->get()->toArray();


//        dd($this->confirmCustomer($input_data));

//        if($customer !== null and $customer_type !== 'PlotsCustomer'){
//            $customer = null;
//            $data['customer_id'] = 0;
//            $flag['error'] = 'wrong customer';
//            $flag['massage'] = 'Customer was '.$customer_type;
//            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');
//
//        }

        return compact('data','customer','customer_type');

    }
    private function mortgageDTO($data_set, $input){
        extract($data_set);
        return [
                       'amount_paid' => $input['amountPaid'],
                       'payment_type' => $customerType,
                       'plot_price' => $plots['price'],
                       'paid_date' => $input['paymentDate']

        ];
    }

    /**
     * @param $data
     */
    private function fullpayment($data){
        $flag = [];
        extract( $this->confirmCustomer($data) );

        if($customer_type == 'PlotsCustomer'){

            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');

        }elseif($customer !== null and $customer_type !== 'PlotsCustomer') {

            $customer = null;
            $data['customer_id'] = 0;
            $flag['warning'] = 'no plot select';
            $flag['massage'] = 'Confirm and select plot';
            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');

        }

        if(isset($data['customer_id']) and $data['customer_id'] !== 0){

            $customer_plots_payments =  $customer->with('plots','payments')->first()->toArray();
            extract($customer_plots_payments);


            if( (empty($payments) or count($payments) != 1) or count($plots) !=  1 ){

//                $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');



            }elseif(count($plots) ==  1 and count($payments) == 1){

                // we can pay the plot and send a confirmation
                $cds = $this->mortgageDTO($customer_plots_payments, $data);
                // we have to do is to put the plot_id in transaction and do the payment after
            }


            // get the customer relationships: plots->payments->lastpayment();

            // calculate the payment using the accounts package

            // add that to an array with key of model

            return compact('data','customer','customer_type', 'flag');

        }

//       $customers =  (object)$customer;
//
//        $help = PlotsCustomer::find($customers->id)->with('plots')->get()->toArray();
//        var_dump($help);
//        $customerPlot = $customer->with('plots')->get()->toArray();


//        dd($this->confirmCustomer($input_data));

//        if($customer !== null and $customer_type !== 'PlotsCustomer'){
//            $customer = null;
//            $data['customer_id'] = 0;
//            $flag['error'] = 'wrong customer';
//            $flag['massage'] = 'Customer was '.$customer_type;
//            $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , true, '-');
//
//        }

        return compact('data','customer','customer_type');

    }
    private function others($data){

        extract($this->confirmCustomer($data));
        $data['paymentDate'] = $this->account->unix_flip($data['paymentDate'] , false, '-');
        // if there customer is selected then now the customer detailas is available one can take anything from the customer
        // remember to merge any relevant data from the set;
        return compact('customer_type','data');

    }

    /**
     * @param $data
     * @return array|\Illuminate\Support\Collection|int|null|static
     */
    private function confirmCustomer($data){
        $customer = [];
        $customer_id  = ( (int)$data['customer_id'] )?: false; $compound = 0 ; $house = 0;

        if($customer_id){


            $customer = $this->customer->getById($customer_id)->first();
            $domain = preg_split("/\\\\/",$customer->customable_type)[1];

           return ['customer_type'=>$domain, 'customer'=>$customer->customable, 'data'=>$data];


        }else {
            if((int)$house){
                $customer  = 0; // House::find((int)$house)->with('tenant.payments')->get();
            }

            if((int)$compound){
                // get the compound
                $customer  = 0; // Compound::find((int)$compound )->with('landlord.payments')->get();
            }
        }

        // if there is no customer

        if(!$customer_id){

            extract($data);
            $info =  ['customer_type' => null,'customer' => null, 'data'=> [
                "title" => $title,
                "description" => $description,
                "paymentMedium" => $paymentMedium,
                "paymentDate" =>$paymentDate,
                "paymentDate" => $this->account->unix_flip($paymentDate, true),
                "category" => $category,
                "revenue" =>$revenue,
                "paymentMedium" => $paymentMedium,
                "amountPaid" =>$amountPaid,
                "total" =>$total

            ]


            ] ;
            return $info;

        }

    }

    public function getById($id){
        return $this->model->find($id);
    }
} 