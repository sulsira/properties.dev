<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 8/1/2015
 * Time: 11:36 PM
 */

namespace Realestate\Account\Transaction;


use App\Transaction;

interface TransactionRepositoryInterface {


    public function save(Transaction $transaction);


    public function creation($data);

    public function getById($id);


} 