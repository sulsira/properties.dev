<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/10/2015
 * Time: 1:23 PM
 */

namespace Realestate;


abstract class DbRepository {


    public function getAll(){
        return $this->model->all();
    }
    
    public function getById($id){
        return $this->model->find($id);
    }

    public function constructDob(array $date){
        // it would take an array or day, month and year build it up to a unix timespampt
    }

    public function destructDob($date){
        //takes a date from database and convert into array
    }

    public function isBlank(array $array, $exception){
        $test_asset = array();
         $dump = [];
        foreach($exception as $x){
            $dump[] = array_pull($array, $x);
        }
        foreach($array as $key => $value){
            if(!empty($value)){
                $test_asset[] = $value;
            }
        }

        return (empty($test_asset))?: false;
    }
} 