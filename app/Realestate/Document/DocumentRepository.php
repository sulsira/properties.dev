<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/19/2015
 * Time: 12:12 AM
 */

namespace Realestate\Document;


use App\Document;
use App\Picture;
use Realestate\DbRepository;

class DocumentRepository extends DbRepository implements DocumentRepositoryInterface{
    protected $model;
    function __construct(Document $document){
        $this->model = $document;
    }
    public function save(Document $document)
    {
        $document->save();
    }

    public function creation($data)
    {
        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;
    }

    public function upload($data)
    {

        $uped = array();
        extract($data);
        $mime = $fileObject->getMimeType() ;

        if(! \File::exists($folder)){

            \File::makeDirectory($folder,  $mode = 0777, $recursive = true);

        }
        if(\File::isDirectory($folder)){
            if(! \File::isDirectory($folder)){
                // the direcotry writeable or something
                // create the directory with privelges
            }
            // prepare information for upload
            $uploaded = \Input::file('document')->move($folder, $fileRename.'.'.$extension);

            if($uploaded){
                $uped = [
                    'title' => $filename,
                    'entity_type' => $entityType,
                    'entity_ID' => $entityID,
                    'type' => 'plot_customer_document',
                    'fullpath' => $folder.'/'.$fileRename.'.'.$extension,
                    'filename' => $fileRename.'.'.$extension,
                    'foldername'=>$folder,
                    'extension' =>$extension,
                    'filetype' => $mime
                ];
            }
        }
        return  $uped;
    }

    public function uploads($data, $filename = 'document')
    {

//

        $uped = array();
        extract($data);

//        dd($fileObject);
        $mime = $fileObject->getMimeType() ;

        if(! \File::exists($folder)){

            \File::makeDirectory($folder,  $mode = 0777, $recursive = true);

        }
        if(\File::isDirectory($folder)){
            if(! \File::isDirectory($folder)){
                // the direcotry writeable or something
                // create the directory with privelges
            }
            // prepare information for upload
            $uploaded = $fileObject->move($folder, $fileRename.'.'.$extension);

            if($uploaded){
                $uped = [
                    'title' => $filename,
                    'entity_type' => $entityType,
                    'entity_ID' => $entityID,
                    'type' => 'plot_customer_document',
                    'fullpath' => $folder.'/'.$fileRename.'.'.$extension,
                    'filename' => $fileRename.'.'.$extension,
                    'foldername'=>$folder,
                    'extension' =>$extension,
                    'filetype' => $mime
                ];
            }
        }
        return  $uped;
    }

} 