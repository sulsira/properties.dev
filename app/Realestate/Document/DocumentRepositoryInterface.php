<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/19/2015
 * Time: 12:13 AM
 */

namespace Realestate\Document;


use App\Document;

interface DocumentRepositoryInterface {
    public function save(Document $document);

    public function creation($data);

    public function upload($data);
} 