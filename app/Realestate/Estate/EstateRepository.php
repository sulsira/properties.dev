<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/7/2015
 * Time: 2:31 AM
 */

namespace Realestate\Estate;


use App\Estate;
use Realestate\DbRepository;

class EstateRepository extends DbRepository implements EstateRepositoryInterface{

    protected  $model;

    public function __construct(Estate $estate){
        $this->model = $estate;
    }
    public function save(Estate $estate){
        return $estate->save();
    }
    public function getById( $id ){
        $id  = (int) $id;
       return $this->model->find($id);
    }

} 