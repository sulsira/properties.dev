<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/10/2015
 * Time: 5:16 AM
 */

namespace Realestate\Estate;


use App\Estate;

interface EstateRepositoryInterface {

    public function save( Estate $estate );

    public function getAll();

} 