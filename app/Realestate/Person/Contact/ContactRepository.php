<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/18/2015
 * Time: 1:18 AM
 */

namespace Realestate\Person\Contact;


use App\Contact;
use Realestate\DbRepository;

class ContactRepository extends DbRepository implements ContactRepositoryInterface {
    protected $model;
    function __construct(Contact $contact){
        $this->model = $contact;
    }



    public function creation($data){
        $set = [];

        $type  = array_pull($data, 'Cont_EntityType');
        $id = array_pull($data, 'Cont_EntityID');

        $data = (array) $data;
       if($this->isBlank($data, ['Cont_EntityType','Cont_EntityID'])) return [];
        foreach($data as $key => $value){
            $set[] = $this->model->create([
                'Cont_EntityID'=> $id,
                'Cont_EntityType' => $type,
                'Cont_Contact' => $value,
                'Cont_ContactType' => $key
            ]);
        }


        return   $set;

    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }
}