<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/18/2015
 * Time: 1:24 AM
 */

namespace Realestate\Person\Contact;


use App\Contact;

interface ContactRepositoryInterface {

    public function getAll();

    public function creation($data);
} 