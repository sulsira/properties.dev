<?php namespace Realestate\Person;
use App\Person;
use Realestate\DbRepository;

/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/13/2015
 * Time: 1:17 PM
 */

class PersonRepository extends DbRepository implements PersonRepositoryInterface{
    protected $model;
    function __construct(Person $person){
        $this->model = $person;
    }

    public function save(Person $person)
    {
        return $person->save();
    }


    public function creation($data){

        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;

    }

}