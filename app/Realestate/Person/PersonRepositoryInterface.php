<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/13/2015
 * Time: 1:19 PM
 */

namespace Realestate\Person;


use App\Person;

interface PersonRepositoryInterface {

    public function save(Person $person);

    public function getAll();

    public function creation($data);
} 