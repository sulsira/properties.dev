<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/18/2015
 * Time: 8:36 PM
 */

namespace Realestate\Person\Picture;


use App\Picture;
use Realestate\DbRepository;

class PictureRepository extends DbRepository implements PictureRepositoryInterface{
    protected $model;
    function __construct(Picture $picture){
        $this->model = $picture;
    }
    public function save(Picture $picture)
    {
        $picture->save();
    }

    public function creation($data)
    {
        $data = (array) $data;

        $entity = $this->model->fill($data);

        return  $entity;
    }

    public function upload($data)
    {
        // base_folder

        // target_folder

        //
        // TODO: Implement upload() method.
    }
}