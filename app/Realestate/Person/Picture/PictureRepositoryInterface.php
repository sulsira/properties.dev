<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/18/2015
 * Time: 8:36 PM
 */

namespace Realestate\Person\Picture;


use App\Picture;

interface PictureRepositoryInterface {

    public function save(Picture $picture);

    public function creation($data);

    public function upload($data);
} 