<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/13/2015
 * Time: 11:35 PM
 */

namespace Realestate\Plot\Customer;


use App\Plot;
use App\PlotsCustomer;
use Realestate\DbRepository;
use Realestate\Plot\PlotRepositoryInterface;

class CustomerRepository extends DbRepository implements CustomerRepositoryInterface{
    protected $model;
    protected $plot;
    function __construct(PlotsCustomer $customer, PlotRepositoryInterface $plot){
        $this->model = $customer;
        $this->plot = $plot;
    }

    public function save(PlotsCustomer $customer)
    {
        return $customer->save();
    }


    public function creation($data){
//        dd($data);
        $data = (array) $data;
        $plot_id = (int)array_pull($data, 'plot_id');
        $entity = $this->model->fill($data);
        return $entity;
    }

    public function customers(){
       return $this->model->customers();
    }

    public function customerWithPlots(){

    }
}