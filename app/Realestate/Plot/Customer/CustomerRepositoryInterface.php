<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/13/2015
 * Time: 11:36 PM
 */

namespace Realestate\Plot\Customer;


use App\PlotsCustomer;

interface CustomerRepositoryInterface {

    public function save(PlotsCustomer $customer);

    public function getAll();

    public function creation($data);
} 