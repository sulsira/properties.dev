<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/17/2015
 * Time: 9:55 AM
 */

namespace Realestate\Plot\Kin;


use App\NextKin;
use Realestate\DbRepository;

class NextKinRepository extends DbRepository implements NextKinRepositoryInterface {
    protected $model;
    function __construct(NextKin $kin){
        $this->model = $kin;
    }

    public function save(NextKin $kin)
    {
        return $kin->save();
    }


    public function creation($data){

        $data = (array) $data;

        if($this->isBlank($data, ['EntityType','EntityID'])) return [];
        $entity = $this->model->fill($data);

        return  $entity;

    }
} 