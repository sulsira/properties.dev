<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/17/2015
 * Time: 9:56 AM
 */

namespace Realestate\Plot\Kin;


use App\NextKin;

interface NextKinRepositoryInterface {
    public function save(NextKin $kin);

    public function getAll();

    public function creation($data);
} 