<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/16/2015
 * Time: 3:39 AM
 */

namespace Realestate\Plot\Payment;


use App\PlotPayment;
use App\PlotsMortgateTransaction;
use Realestate\DbRepository;
use Sulsira\Accounts\Accounts;

class PaymentRepository extends DbRepository implements PaymentRepositoryInterface{
    protected $model;
    protected $account;
    public $data_mapper;
    function __construct(PlotPayment $payment, Accounts $account){
        $this->model = $payment;
        $this->account = $account;
    }

    public function save(PlotPayment $payment)
    {
        return $payment->save();
    }


    public function creation($data){


        $data = (array) $data;

        $data = $this->prepare($data);

        if(empty($data)) return [];

        $entity = $this->model->fill($data);

        return  $entity;

    }

    /**
     * this method is in charge of paying the account wether is it mortgage or fullpayment
     * @param $data
     * @return mixed
     */
    public function paying($data) {

        extract($data);

        if($paymentType == 'fullpayment'){

           return  $this->account->plotSale()->pay($this->plotsaleDTO($data))->getPayment();

        }

        return $this->account->mortgage()->pay($this->mortgageDTO($data))->getPayment();
    }

//    public  function transactions($data){
//        var_dump($data);
//        $results =  $this->model->fill(['downPayment'=>200]);
//        dd($results);
////        \DB::transaction(function()use ($data) {
////
////        });
//    }

     protected function mortgageDTO($data){
         extract($data);
         return [
               'amount_paid' => null,
               'total_paid' => $totalPayment,
               'payment_due'=> $duePayment,
               'payment_duration_in_months'=> $paymentPeriodInMonths,
               'calculated_monthly_fee' => 0,
               'number_of_months_paid' => 0,
               'unhindered_closing_payment_date'=> null,
               'next_payment_date' => null,
               'commencement_date' => $paymentCommencementDate,
               'payment_type' => $paymentType,
               'down_payment' => $downPayment,
               'plot_price' => null,
               'paid_date' => $dateOfPayment,
               'arrears' => null,
               'balance' => null

         ];
     }

    protected function plotsaleDTO($data){

        extract($data);
        return [
            'amount_paid' => $totalPayment,
            'payment_type' => $paymentType,
            'paid_date' =>$dateOfPayment
        ];
    }

    protected function prepare($data){
         $test_asset = [];

        foreach($data as $key => $value){
            if(!empty($value)){
                if($key != 'plot_customer_id' and $key != 'plot_id' and $key != 'paymentType'){
                    $test_asset[] = $value;
                }

            }
        }

        if(empty($test_asset)) return [];

        $plots_payments = [];
        extract($data);
        // this return an array where the keys are models and value are key paired values for database table attributes
        $data_set = $this->paying($data);
//        var_dump($data);
//        dd($data_set);
        if($paymentType == 'fullpayment'){

            $plots_payments = [
                'plot_id' => $plot_id,
                'user_id' => 0,
                'agent_id' => 0 ,
                'plot_customer_id' => $plot_customer_id,
                'paymentType' => $paymentType,
                'transaction' => 'income',
                'paymentStatus' => 'completed',
                'dateOfPayment' =>  $data_set['dateOfPayment'],
                'totalPayment' =>  $data_set['total_paid']
            ];

        }else{

            $plots_payments = [
                'plot_id' => $plot_id,
                'user_id' => 0,
                'agent_id' => 0 ,
                'plot_customer_id' => $plot_customer_id,
                'paymentType' => $paymentType,
                'transaction' => 'income',
                'paymentStatus' => 'open',
                'downPayment' => $downPayment,
                'duePayment' => $data_set['payment_due'],
                'dateOfPayment' =>  $dateOfPayment,
                'totalPayment' =>  $data_set['total_paid'],
                'paymentPeriodInMonths' =>  $paymentPeriodInMonths,
                'monthlyFee' =>  $data_set['calculated_monthly_fee'],
                'balance' =>  $data_set['balance'],
                'arrears' =>  $data_set['arrears'],
                'paymentCommencementDate' => $data_set['paymentCommencementDate'],
                'normalClosePaymentDate' => $data_set['unhindered_closing_payment_date'],
                'dateOfPayment' => $data_set['dateOfPayment']
            ];

        }

        return $plots_payments;

    }

    public function setDataMapper(){
        return $this->data_mapper =  [
            "paymentPeriodInMonths" ,
            "paymentType" ,
            "paymentCommencementDate" ,
            "downPayment",
            "totalPayment" ,
            "dateOfPayment" ,
            "duePayment",
            "plot_customer_id" ,
            "plot_id"
        ];
    }
} 