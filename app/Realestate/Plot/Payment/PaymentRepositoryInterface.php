<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/16/2015
 * Time: 3:57 AM
 */

namespace Realestate\Plot\Payment;


use App\PlotPayment;

interface PaymentRepositoryInterface {
    public function save(PlotPayment $payment);

    public function getAll();

    public function creation($data);

    public function setDataMapper();
} 