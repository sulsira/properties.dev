<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/7/2015
 * Time: 4:09 PM
 */

namespace Realestate\Plot;


use App\Estate;
use App\Plot;
use App\PlotsCustomer;
use Realestate\DbRepository;

class PlotRepository extends DbRepository implements PlotRepositoryInterface {
    protected $model;
    public function __construct(Plot $plot){
        $this->model = $plot;
    }

    public function save(Plot $plot){
        return $plot->save();
    }

    public function available(){
        return $this->model->available()->lists('indentification','id');
    }

    public function creation($data){

        $data = (array) $data;
        $data['plot_customer_id'] = 999999999;
        $data['agent_id'] = 999999999;
        if( (int) $data['plot_customer_id'] ){
            $data = array_add($data, 'status', 0);
        }else{
            $data = array_add($data, 'status', 1);
        }

        $entity = $this->model->fill($data);

        return  $entity;

    }

    public function getById( $id ){
        $id  = (int) $id;
        return $this->model->find($id);
    }
} 