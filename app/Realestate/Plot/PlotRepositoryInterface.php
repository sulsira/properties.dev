<?php
/**
 * Created by mamadou.
 * User: mamadou
 * Date: 7/13/2015
 * Time: 11:34 PM
 */

namespace Realestate\Plot;


use App\Plot;

interface PlotRepositoryInterface {
    public function save(Plot $plot);

    public function available();

    public function getById( $id );

    public function creation($data);
} 