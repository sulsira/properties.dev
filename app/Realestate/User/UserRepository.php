<?php
/**
 * User: mamadou
 * Date: 4/15/2015
 * Time: 4:15 PM
 */

namespace Realestate\User;

use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Auth;

class UserRepository {
    public $prev;

    public function save(User $user){
        return $user->save();
    }

    public function role(UserRole $role){
        return $role->save();
    }

    /**
     *
     * @return $this
     */
    public function authorizeTo(){
        $this->prev = session('previledges');
        return $this;
    }
    public function add(){
        if( strlen($this->prev) == 4){
            return true;
        }
        return (str_contains($this->prev, 'a'));
    }
    public function view(){
        if( strlen($this->prev) == 4){
            return true;
        }
        return (str_contains($this->prev, 'v'));
    }
    public function edit(){
        if( strlen($this->prev) == 4){
            return true;
        }
        return (str_contains($this->prev, 'e'));
    }
    public function delete(){
        if( strlen($this->prev) == 4){
            return true;
        }
        return (str_contains($this->prev, 'd'));
    }
} 