<?php

if(! function_exists('list_customers_with_entities')):

    function list_customers_with_entities(array $data){
        $set = [];

        foreach($data as $key => $val){

            foreach($val as $k=>$value){

                if(!empty($value['plots'])){
                    $tes = $value['plots'];
                    $i = 0;
                    $st = ' ( ';
                    foreach( $tes as $id => $ent ){

                        if(count($tes) -1  >= $i ){
                            $st .= ($i == 0)? '' : ',';

                        }
                        $st .= $ent;
                        ++$i;
                    }
                    $st .= ' )';

                }

                $set[$key][$value['customer_id']] = $value['fullname']. ($st) ;

            }
        }
        return  $set;
    }
endif;

