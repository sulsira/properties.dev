<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Receipt extends Model {

	//
   protected  $fillable = ['user_id', 'entity_id', 'entityType', 'description', 'paymentType', 'amount', 'receptDate', 'bankAccount_id', 'source', 'account', 'category', 'reference', 'file'];

    public function setReceptdateAttribute($date){
        $this->attributes['receptDate'] = Carbon::parse($date);
    }
//    public function setIssueddateAttribute($date){
//        $this->attributes['issuedDate'] = Carbon::parse($date);
//    }
}
