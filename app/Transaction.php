<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $fillable = ['title', 'payment_for', 'revenue','receipt_number','bankable', 'bankable_id', 'bank_cheque', 'category', 'description', 'amountPaid', 'paymentDate', 'income', 'paymentMedium', 'documentUrl', 'customer_id', 'total', 'plot_id', 'user_id', 'approval_status', 'deleted'];

    public function getFields(){
        return $this->fillable;
    }

    public function plots(){
        return $this->belongsTo('App\Plot', 'plot_id', 'id');
    }
    public function rent(){
//        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }
    public function customer(){

        return $this->belongsTo('App\Customer', 'customer_id', 'id');

    }
}
