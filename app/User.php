<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\UserRole;
use App\UserProperties;
use Illuminate\Support\Facades\Hash;

//use Illuminate\Support\Facades\Hash;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	// protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	protected $fillable = [
	'email','password','access_codesID', 'username','status'];


    /**
     * @param $password
     */
    public function setPasswordAttribute($password){
		
		$this->attributes['password'] = \Hash::make($password);

	}
	public function role(){
		return $this->hasOne('App\UserRole', 'user_id', 'id');
	}
	public function payments(){
		return $this->hasMany('Payment','paym_userID','id');
	}

	public function property(){
		return $this->hasOne('App\UserProperties', 'user_id', 'id');
	}
    public static function creation($data){

        $user = new static($data);
        return $user;

    }
}
