<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserRole extends Model {

	protected $table = "userroles";
	protected $primaryKey = "id";
	protected $fillable = [
							'user_id',
							'sign_id',
							'previledges',
							'security_level',
							'domain',
							'usergroup',
							'department_id',
							'url',
							'fullname',
							'phone',
							'department',
                            'thumbnail',
                            'image',
                            'filename'
						];

	public function role(){
		return $this->belongsTo('App\User','user_id');
	}

	public static function creation($data){
	   $user = new static($data);
	   return $user;
	}


}
