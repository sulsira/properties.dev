<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estates', function(Blueprint $table) {
			
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('location')->nullable();
            $table->string('lon')->nullable();
            $table->string('lat')->nullable();
            $table->timestamps();

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estates');
	}

}
