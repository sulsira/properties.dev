<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plots', function(Blueprint $table) {

            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            $table->increments('id');
            $table->integer('estate_id')->unsigned()->index();
            $table->integer('agent_id')->unsigned()->index();
            $table->integer('plot_customer_id')->default(0)->unsigned()->index();
            $table->string('name')->nullable();
            $table->string('indentification')->nullable();
            $table->string('size')->nullable();
            $table->decimal('price', 10, 4)->default(0);
            $table->string('lon')->nullable();
            $table->string('lat')->nullable();
            $table->integer('status')->default(0);
            $table->enum('availability', ['available','un-available','sold','others']);
            $table->decimal('total_payment', 10, 4)->default(0);
            $table->enum('purchaseType',['Mortgage','sold','lease','rent', 'others','individual']);
            $table->string('remarks')->nullable();
            $table->timestamps();


            // $table->foreign('estate_id')
            //       ->references('id')->on('estates')
            //       ->onDelete('cascade');

           $table->foreign('estate_id')->references('id')->on('estates');
           // $table->foreign('agent_id')->references('id')->on('agents');
           

           \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plots');
	}

}
