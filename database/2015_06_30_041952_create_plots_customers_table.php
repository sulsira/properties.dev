<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotsCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plots_customers', function(Blueprint $table) {
			 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $table->increments('id');
            $table->integer('plot_id')->unsigned()->index();
            $table->integer('partner_id')->unsigned()->index()->default(0);
            $table->integer('person_id')->unsigned()->index();
            $table->integer('payment_id')->unsigned()->index();
            $table->enum('customerType',['mortgage','fullpayment']);
            $table->integer('status')->default(0);
            $table->timestamps();


			// $table->foreign('estate_id')->references('id')->on('estates');
            $table->foreign('plot_id')->references('id')->on('plots');
            // $table->foreign('partner_id')->references('id')->on('partners');
            $table->foreign('person_id')->references('id')->on('persons');
            $table->foreign('payment_id')->references('id')->on('plots_payments');


             DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plots_customers');
	}

}
