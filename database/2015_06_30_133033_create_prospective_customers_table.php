<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectiveCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prospective_plot_customers', function(Blueprint $table) {
			 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $table->increments('id');
           	$table->string('fullname')->nullable();
           	$table->string('location')->nullable();
           	$table->integer('agent_id')->default(0);
           	$table->integer('plot_id')->default(0);
           	$table->string('phone')->nullable();
           	$table->string('remarks')->nullable();
            $table->timestamps();
             DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prospective_plot_customers');
	}

}
