<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatesNotificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estates_notification', function(Blueprint $table) {
			 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $table->increments('id');
             $table->integer('plot_id')->default(0);
             $table->integer('agent_id')->default(0);
             $table->integer('customer_id')->default(0);
             $table->string('notificationType')->nullable();
             $table->string('status')->nullable();
             $table->string('notificationTitle')->nullable();
             $table->string('notificationMessage')->nullable();
             $table->string('confirmed')->nullable();
             $table->string('confirmedBy')->nullable();
             $table->boolean('deleted')->default(0);
            $table->timestamps();
             DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estates_notification');
	}

}
