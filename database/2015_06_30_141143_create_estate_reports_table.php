<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstateReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estate_reports', function(Blueprint $table) {
			 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('graphType')->nullable();
            $table->string('description')->nullable();
            $table->string('name')->nullable();
            $table->string('tableName')->nullable();
            $table->timestamps();
             DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estate_reports');
	}

}
