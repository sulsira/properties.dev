<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plots_payments', function(Blueprint $table) {
             DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $table->increments('id');
            $table->integer('plot_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('agent_id')->unsigned()->index();
            $table->integer('plot_customer_id')->unsigned()->index();
    		$table->enum('paymentType',['mortgage','fullpayment']);
    		$table->enum('transaction',['income','expense']);
    		$table->enum('paymentStatus',['open','pending','closed']);
    		$table->decimal('downPayment',10,4)->default(0);
    		$table->decimal('totalPayment',10,4)->default(0);
    		$table->integer('paymentPeriodInMonths')->default(0);
    		$table->decimal('monthlyFee',10,4)->default(0);
    		$table->decimal('balance',10,4)->default(0);
    		$table->decimal('arrears',10,4)->default(0);
    		$table->date('paymentCommencementDate')->timestamps();
            $table->timestamps();

            $table->foreign('plot_id')->references('id')->on('plots');
            // $table->foreign('agent_id')->references('id')->on('agents');

            $table->foreign('plot_customer_id')->references('id')->on('plots_customers');    
             DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estate_payments');
	}

}
