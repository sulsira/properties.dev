<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotsMortgateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plots_mortgage_transactions', function(Blueprint $table) {
 			DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $table->increments('id');

            $table->integer('plot_id')->unsigned()->index();

            $table->integer('user_id')->unsigned()->index();

            $table->integer('plot_payment_id')->unsigned()->index();

            $table->integer('plot_customer_id')->unsigned()->index();

            $table->decimal('amount_paid', 10 , 3)->default(0);

            $table->date('datePaidFor')->timestamps();
            $table->string('monthPaidFor')->nullable();
            $table->integer('yearOfPayment')->default(0);
            $table->boolean('deleted')->default(0);
            $table->timestamps();


            $table->foreign('plot_id')->references('id')->on('plots');

            $table->foreign('plot_payment_id')->references('id')->on('plots_payments');

            
            
            $table->foreign('plot_customer_id')->references('id')->on('plots_customers'); 

 			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
              
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plots_mortgate_transactions');
	}

}
