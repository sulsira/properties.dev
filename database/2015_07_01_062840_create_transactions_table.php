<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table) {
            $table->increments('id');
            $table->string("for")->nullable();
            $table->string("description")->nullable();
            $table->decimal("amountPaid", 8, 2)->default(0);
            $table->date("paymentDate")->nullable();
            $table->string("income")->nullable();
            $table->string("paymentMedium")->nullable();
            $table->string("documentUrl")->nullable();
            $table->integer("customer_id")->default(0);
            $table->integer("user_id")->default(0);

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
