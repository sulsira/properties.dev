<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->index();
            $table->integer("entity_id")->default(0);
            $table->integer("entityType")->default(0);
            $table->string("description")->nullable();
            $table->string("paymentType")->nullable();
            $table->decimal('amount', 8, 3)->default(0);
            $table->date('receptDate')->timestamps();
            $table->integer("bankAccount_id")->default(0);
            $table->string("source")->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipts');
	}

}
