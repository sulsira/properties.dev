<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function(Blueprint $table) {



            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('name')->nullable();
            $table->string('entity_type')->nullable();
            $table->string('entity_ID')->nullable();
            $table->string('type')->nullable();
            $table->string('fullpath')->nullable();
            $table->string('filename')->nullable();
            $table->string('foldername')->nullable();
            $table->string('extension')->nullable();
            $table->string('filetype')->nullable();
            $table->string('thumnaildir')->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();




        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('images');
	}

}
