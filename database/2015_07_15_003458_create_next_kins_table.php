<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNextKinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('next_kins', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('EntityID')->default(0);
            $table->string('EntityType')->nullable();
            $table->string('fullname')->nullable();
            $table->string('contacts')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('next_kins');
	}

}
