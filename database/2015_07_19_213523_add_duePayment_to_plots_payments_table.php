<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDuePaymentToPlotsPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plots_payments', function(Blueprint $table) {
            $table->decimal('duePayment', 8, 3)->default(0)->after('downPayment');
            $table->datetime('dateOfPayment')->timestamp()->after('duePayment');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plots_payments', function(Blueprint $table) {
            $table->dropColumn('duePayment');
             $table->dropColumn('dateOfPayment');
        });
	}

}
