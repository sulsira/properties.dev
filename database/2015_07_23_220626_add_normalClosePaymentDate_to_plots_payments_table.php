<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNormalClosePaymentDateToPlotsPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plots_payments', function(Blueprint $table) {
            $table->date('normalClosePaymentDate')->timestamp('datetime')->after('arrears');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plots_payments', function(Blueprint $table) {
            $table->dropColumn('normalClosePaymentDate');
        });
	}

}
