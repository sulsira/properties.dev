<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManypcustomerrelationshipToPlotsCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plots_customers', function(Blueprint $table) {
            $table->foreign('payment_id')->references('id')->on('plots_payments');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plots_customers', function(Blueprint $table) {
            
        });
	}

}
