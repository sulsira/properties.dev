<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlotPlotscustomerPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plot_plotscustomer', function(Blueprint $table)
		{
			$table->integer('plot_id')->unsigned()->index();
			$table->foreign('plot_id')->references('id')->on('plots')->onDelete('cascade');
			$table->integer('plots_customer_id')->unsigned()->index();
			$table->foreign('plots_customer_id')->references('id')->on('plots_customers')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plot_plotscustomer');
	}

}
