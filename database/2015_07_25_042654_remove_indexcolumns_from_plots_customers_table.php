<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveIndexcolumnsFromPlotsCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plots_customers', function(Blueprint $table) {
            $table->dropForeign('plots_customers_plot_id_foreign');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plots_customers', function(Blueprint $table) {
            
        });
	}

}
