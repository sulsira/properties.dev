<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReidsToPlotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plots', function(Blueprint $table) {
        			 $table->integer('plot_customer_id')->default(0)->after('estate_id');
            $table->integer('agent_id')->default(0)->after('plot_customer_id');    
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plots', function(Blueprint $table) {
                       $table->dropColumn('plot_customer_id');
             $table->dropColumn('agent_id'); 
        });
	}

}
