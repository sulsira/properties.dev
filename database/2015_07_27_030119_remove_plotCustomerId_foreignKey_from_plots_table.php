<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePlotCustomerIdForeignKeyFromPlotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plots', function(Blueprint $table) {
            $table->dropForeign('plots_estate_id_foreign');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plots', function(Blueprint $table) {
            $table->foreign('plot_customer_id')
                  ->references('id')->on('plots_customers')
                  ->onDelete('cascade');
        });
	}

}
