<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function(Blueprint $table) {
            $table->string('title')->nallable()->after('id');
            $table->string('revenue')->nullable()->after('payment_for');
            $table->string('bankable')->nullable()->after('revenue');
            $table->integer('bankable_id')->default(0)->after('bankable');
            $table->string('bank_cheque')->nullable()->after('bankable_id');
            $table->string('category')->nullable()->after('bank_cheque');
            $table->decimal('total',8,3)->nullable()->after('customer_id');
            $table->integer('plot_id')->default(0)->after('total');
            $table->boolean('approval_status')->default(0)->after('user_id');	
            $table->boolean('deleted')->default(0)->after('approval_status');	
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function(Blueprint $table) {
            
        });
	}

}
