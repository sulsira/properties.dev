<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table) {
            $table->increments('id');
            $table->string("productService")->nullable();
            $table->string("description")->nullable();
            $table->integer("quantity")->default(0);
            $table->decimal("amount", 10, 2)->default(0);
            $table->date("dueDate")->timestamps();
            $table->date("issuedDate")->timestamps();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
