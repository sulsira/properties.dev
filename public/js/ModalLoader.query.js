(function($, window, document,undefined){
// utility
	if(typeof Object.create !== 'function'){
	  Object.create = function( obj ){
	    function F(){};
	    F.prototype = obj;
	    return new F();
	  };
	}
	var	Loader = {

		init: function(options, elem){
			var self = this;
			self.$elem = $(elem);
			self.elem = elem;

			if (typeof options === 'string') {
				// when the page is found and everything is simple and intact
				self.url = options;

			}else{

				// when an array is return and more processing is required
				self.options = $.extend({}, $.fn.modalLoader.options, options);
				self.url = self.options.url;
			}

			// // EVENT TRIGGERS BELLOW
			self.eventHander.click.call(this);

		},

		eventHander: {
			
			click: function(){
				var self = this, options = self.options;
				self.options.trigger.on('click',function(e){
					e.preventDefault();
					self.buildFilename($(this));
					self.process();
				});

			}
		},

		process: function(){
			var self = this, options = self.options;
			// this acts like a constructor and its process all other methods

			self.fetch().done(self.display);

		},
		fetch: function(){
			var self = this;
			// // returns a ajax load of a page
			return $.ajax({
				url: self.url,
				data: {id:self.ID},
				type: 'GET',
				dataType: 'JSON',
				beforeSend: self.loaderTrigger,
				success: function(data){
					self.templater(data);
				},
				error:function(e,m){
					console.log(m);
				}
			});
		},
		templater: function(data){
			var self = this, options = self.options;
			// it builds up an html template with the data

				options.platForm.html(data);

			
		},

		display: function(data){
			var options = $.fn.modalLoader.options;
			var loader = options.loader, progress = loader.find('.progress'), id = $(options.platForm).children().first().data('id');
			// //displays the loader 
			
			// console.log(self.options.loader);
	
			for (var i = 10; i <= 100; i++) {


				progress.find('.progress-bar').css('width', i + '%');

			};

			// var options = $.fn.modalLoader.options
			// it displays the templated html fragment into the dom
			// console.log(data);
			// console.log($(id).show());

			$(id).modal();
			$.publish('modal/loaded');

			$(id).on('hidden.bs.modal', function(){

				progress.find('.progress-bar').css('width', '0'+'%');
				loader.hide();

			if (   ~~(options.platForm).find(id).length == true ) {
				
				(options.platForm).find(id).detach();
			}


			});
		},

		loaderTrigger: function(data){


			var options = $.fn.modalLoader.options;
			var loader = options.loader, progress = loader.find('.progress');
			// //displays the loader 
			progress.find('.progress-bar').css('width', '0'+'%');
			$(loader).show();

			// console.log(self.options.loader);
			for (var i = 0; i <= 8; i++) {

				progress.find('.progress-bar').css('width', i + '%');
			};
			// console.log(this);
			//make the width  to zero

			// incrementally increase the width
		},
		modalTrigger: function(){
			var options = $.fn.modalLoader.options;
			var loader = options.loader, progress = loader.find('.progress');
			// //displays the loader 
			// progress.find('.progress-bar').css('width', '0'+'%');
			

			// console.log(self.options.loader);
			// for (var i = 100; i >= 0; --i) {
			// 	progress.find('.progress-bar').css('width', i + '%');
			// };
			$(loader).hide();
				
		},
		buildFilename: function(data){
			var self = this;
			//return the name of the file to be loaded by ajax
			self.ID = data.data(self.options.dataName);
			self.url = $(data).attr('href');
		},
		getID: function(){
			return this;
		}

	};

	$.fn.modalLoader = function( options ){
		return this.each(function(){
			var loader = Object.create( Loader );
			loader.init(options, this);
		});
	};

	$.fn.modalLoader.options = {

		container: $('.add-nav'),
		containerChild: 'ol',
		trigger: $('a.adder'),
		dataName: 'target',
		loader: $('div.loader'),
		platForm: $('div.platform'),
		url: null

	};

})(jQuery, window, document);

