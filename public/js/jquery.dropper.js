(function($, window, document,undefined){
	// utility
	if(typeof Object.create !== 'function'){
	  Object.create = function( obj ){
	    function F(){};
	    F.prototype = obj;
	    return new F();
	  };
	}
	var Selector = {
		init: function(options, elem){
			var self = this;
			var $this = $(this);
			self.options = $.extend({}, $.fn.dropper.options, options);
			self.options = $.extend({}, $.fn.dropper.options, options);
			self.selector = self.options.selector
			self.container = self.options.container
			self.eventHandler.select.call(this);
			// hide the defautl elements from showing;



		},
		eventHandler: {
			select: function(){

				var self = this;
				var selec = self.selector;

				this.selector.on('change', function(e){

					self.option = $(this).find('option:selected').val();

					self.process();

				});
			}
		},
		process: function(){
			
			$('.'+this.option)
							.show()
							.siblings()
							.hide()
							.end();

		}
	}



// add a new namespace in the jquery namespace.
	$.fn.dropper = function( options ){

			var selector = Object.create( Selector );

			selector.init(options, this);

			return selector;

	};

	$.fn.dropper.options = {
		selector: $('.dropper-selector'),
		container: $('.selector-container')

	};
})(jQuery, window, document);

