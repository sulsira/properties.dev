/**
 * Created by mamadou on 7/8/2015.
 */
(function( $ ){

    var o = $( {} );

    $.each({
        trigger: 'publish',
        on: 'subscribe',
        off: 'unsubscribe'
    }, function(key, val){

        jQuery[val] = function(){

            o[key].apply(o, arguments);

        };


    });


})(jQuery);


