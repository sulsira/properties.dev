(function($){
	var Selector = {
		init: function(config){
			var self = this;
			var $this = $(this);
			this.selector = config.selector;
			this.dropper = config.dropper;
			self.eventHandler.select.call(this);
			this.dropper.find('.fullpayment').hide();
		},
		eventHandler: {
			select: function(){
				var self = this;
				var selec = self.selector;
				this.selector.on('change', function(e){
					self.option = selec.find('option:selected').val();
					self.process();
				});
			}
		},
		process: function(){
			$('.'+this.option)
							.show()
							.siblings()
							.hide()
							.end();

		}
	}
	Selector.init({
		'selector': $('#customerType'),
		'dropper': $('#dropper')
	})
})(jQuery);