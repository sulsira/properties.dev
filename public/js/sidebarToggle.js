(function($){

var stb = {

	init: function(data){

		this.container = data.container, this.department =  data.department, this.menu = data.menu;
		this.system_link = $(this.container).children("li").first().find("a");

		this.eventsBinder.click.call(this);
	},

	eventsBinder: {
		click: function(e){
			var $this = this;
			$this.system_link.on('click', $this.system);
			$this.department.on('click', $this.departments);
		}
	},
	hider: function(element){

		$.each(element, function(index, value){

			$(value).find('a') .slideUp(300);
		});


	},

	shower: function(element){

		var $this = stb;
		$.each(element, function(index, value){

			$(value).find('a') 
							.slideDown(200);

		});
		return $(this);

	},

	animation: function(){

	},
	zap: function(){

	},
	system: function(e){

	var $this = stb, li = $(this).parent();
	var elems = $(this).parent().siblings();
	e.preventDefault();

		if ( li.hasClass('showing') ) {

			$this
				.hider(elems); 

			li.removeClass("showing");

			return this;

		};

		li.addClass('showing');
		$this
			.shower(elems);

	},

	departments: function(e){
		var self = stb, ul = $(this).find((self.menu));
		e.preventDefault();

		if ( ul.is(":hidden") ) {

			ul.slideDown(200);

		}else{

			ul.slideUp();
				
		}

	}
};



stb.init({
	container: $('ul#systems'),
	department: $('li.system'),
	menu: $('.system-menu')		
});
})(jQuery);



