
			<div class="form-group">
					<label class="control-label">
						Fullname:
					</label>
					<hr>
					<div class="input-group">
						<div class="input-group-addon">First </div>
						{!!Form::text('pers_fname',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Middle </div>
						{!!Form::text('pers_mname',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Last </div>
						{!!Form::text('pers_lname',null,['class'=>'form-control'])!!}
					</div>
			</div>
			<hr>
			<div class="form-group">
					<label class="control-label">
						Contacts:
					</label>
					<hr>
					<div class="input-group">
						<div class="input-group-addon">Phones </div>
						{!!Form::text('contacts[phones]',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Emails </div>
						{!!Form::text('contacts[emails]',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Address </div>
						{!!Form::text('contacts[address]',null,['class'=>'form-control'])!!}
					</div>
			</div>
			<hr>


				<div class="form-group">
		            <label class="control-label">
		              Customer Details:
		            </label>
		            <hr>
		            <div class="form-group clearfix" id="plot-estate-details">
        					<div class="input-group">
        						<div class="input-group-addon">Customer Type</div>
                            {!!Form::select('customer[customer_type]',['mortgage'=>'mortgage', 'fullpayment'=>'plot sale', 'rent'=>'Rent', 'others'=>'Others'], null, array('class'=>'select-estate form-control' , 'id'=>'customerType'));!!}
        					</div>
        					<div class="input-group">
        						<div class="input-group-addon">payment Type</div>
            {!!Form::select('customer[payment_type]',['cash'=>'cash', 'bank'=>'bank', 'cheque'=>'cheque', 'misc'=>'misc'], null, array('class'=>'dropper-selector form-control' , 'id'=>'paymentType'));!!}
        					</div>
        					<div class="input-group">
        						<div class="input-group-addon">Account</div>
						{!!
							Form::select( 

								'customer[account]',

								['Income'=>'Income', 'Expense'=>'Expense', 'Others'=>'Others'],

								null,

								array(
									'class'=>'form-control',
									'id'=>'account'
									)

								);
						!!}
        					</div>

        			<div class="selector-container">
        				<div class="cash">
        									<div class="input-group">
						<div class="input-group-addon">Receipt # </div>
						{!!Form::text('customer[receipt_number]',null,['class'=>'form-control','placeholder'=>'receipt or cheque #'])!!}
					</div>	
        				</div>

        				<div class="cheque" style="display:none">
        					<div class="input-group">
						<div class="input-group-addon">Cheque # </div>
						{!!Form::text('customer[cheque]',null,['class'=>'form-control','placeholder'=>'receipt or cheque #'])!!}
					</div>
					        					<div class="input-group">
        						<div class="input-group-addon">Bank Account</div>
            {!!Form::select('customer[cheque_bank_account]',$banks, null, array('class'=>'dropper-selector form-control' , 'id'=>'paymentType'));!!}
        					</div>
					
        				</div>


        				<div class="bank" style="display:none">
					        					<div class="input-group">
        						<div class="input-group-addon">Bank Account</div>
                            {!!Form::select('customer[bank_account]',$banks, null, array('class'=>'select-estate form-control' , 'id'=>'customerType'));!!}

        					</div>
        				</div>
        				<div class="misc">
        					        					<div class="input-group">
						<div class="input-group-addon">Reference # </div>
						{!!Form::text('customer[reference]',null,['class'=>'form-control','placeholder'=>'receipt or cheque #'])!!}
					</div>
        				</div>
        			</div>

		            </div>

				</div>

				<hr>

				<div class="form-group">
					<label class="control-label">
		              Payment Details:
		            </label>
		            <hr>
		            <div id="dropper">
		            	<div class="fullpayment">
		            								<div class="input-group">
        						<div class="input-group-addon">Plot </div>
        						{!!Form::select('payment[Plot_fullpayment][plot_id]', $plots, null, array('class'=>'select-estate form-control'));!!}
        					</div>
							<div class="input-group">
								<div class="input-group-addon">Amount Paid </div>
								{!!Form::text('payment[Plot_fullpayment][amount_paid]',null,['class'=>'form-control'])!!}
							</div>
							<div class="input-group">
								<div class="input-group-addon">Description </div>
								{!!Form::text('payment[Plot_fullpayment][description]',null,['class'=>'form-control'])!!}
							</div>
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Date of Payment :</span>
				                    <span>
				                        {!!Form::number('f[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::number('f[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('f[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>
		            	</div>
		            	<div class="mortgage">
							<div class="input-group">
        						<div class="input-group-addon">Plot </div>
        						{!!Form::select('payment[Plot_mortgage][plot_id]', $plots, null, array('class'=>'select-estate form-control'));!!}
        					</div>
							<div class="input-group">
								<div class="input-group-addon">Down Payment </div>
								{!!Form::text('payment[Plot_mortgage][down_payment]',null,['class'=>'form-control'])!!}
							</div>

							<div class="input-group">
								<div class="input-group-addon">Due Payment </div>
								{!!Form::text('payment[Plot_mortgage][due_payment]',null,['class'=>'form-control'])!!}
							</div>
	

							<div class="input-group">
								<div class="input-group-addon">Period In Months</div>
								{!!Form::text('payment[Plot_mortgage][payment_period_in_months]',null,['class'=>'form-control'])!!}
								{{--<p class="help-block">Payment in months</p>--}}
							</div>

							<div class="input-group">
								<div class="input-group-addon">Payment Commencement Date</div>
								{!!Form::text('payment[Plot_mortgage][payment_commencement_date]',null,['class'=>'form-control','placeholder'=>'mm-dd-year,eg: 1-28-2014. all in numbers'])!!}

							</div>
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Date of Payment :</span>
				                    <span>
				                    {!!Form::number('m[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('m[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('m[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>

		            	</div>
		            	<div class="rent" style="display:none">

							<div class="input-group">
								<div class="input-group-addon">Description</div>
								{!!Form::text('payment[rent][description]',null,['class'=>'form-control'])!!}
							</div>

							<div class="input-group">
								<div class="input-group-addon">Payment </div>
								{!!Form::text('payment[rent][payment]',null,['class'=>'form-control'])!!}
							</div>
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Date of Payment :</span>
				                    <span>
				                    {!!Form::number('r[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('r[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('r[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>

		            	</div>
		            	<div class="others" style="display:none">

							<div class="input-group">
								<div class="input-group-addon">Description</div>
								{!!Form::text('payment[others][description]',null,['class'=>'form-control'])!!}
							</div>

							<div class="input-group">
								<div class="input-group-addon">Payment </div>
								{!!Form::text('payment[others][payment]',null,['class'=>'form-control'])!!}
							</div>
							<div class="input-group">
								<div class="input-group-addon">For </div>
								{!!Form::text('payment[others][for]',null,['class'=>'form-control'])!!}
							</div>
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Date of Payment :</span>
				                    <span>
				                    {!!Form::number('o[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('o[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('o[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>

		            	</div>

		            </div>
					
				</div>
				<hr>

				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
				</div><!-- end of modal footer -->
