
<table class="datatify table table-striped">
	<caption><strong>all customers</strong></caption>
	<thead>
		<tr>
			<th>#</th>
			<th>Fullname</th>
			<th>Type</th>
			<th>location</th>
			<th>Phone</th>
			<th>Income</th>
			<th>Expense</th>
			<th>status</th>
			<th>date created</th>
		</tr>
	</thead>
	<tbody>
		@if(is_null($data))

			<tr>
				<td colspan="9"><strong>sorry there is no data recorded yet!</strong></td>
			</tr>

		@else:
			<?php $i = 0; ?>
			@foreach($data as $domain => $customers)
				@foreach($customers as $index => $customer)

					<tr>
						<td>{!! $i + 1 !!}</td>
						<td><a href="#">{!! ucwords($customer->person->pers_fname .' '.$customer->person->pers_mname.' '.$customer->person->pers_lname ) !!}</a></td>
						<td>{!! $customer->customerType !!}</td>
						<td>rent</td>
						<td>Phone</td>
						<td>Income</td>
						<td>Expense</td>
						<td>status</td>
						<td>date created</td>
					</tr>
					<?php $i++; ?>
				@endforeach

			@endforeach
		@endif
	</tbody>
</table>

