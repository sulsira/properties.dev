<div class="modal-body">
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon">Estate Name</div>
      {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter estate name'])!!}
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon">Location </div>
      {!!Form::text('location',null,['class'=>'form-control','placeholder'=>'Enter location'])!!}
    </div>
  </div>   
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
</div><!-- end of modal footer -->    