<div class="form-group">
	<label class="control-label">
		Expense:
	</label>
	<hr>
	<div class="input-group">
		<div class="input-group-addon">Description </div>
		{!!Form::text('description',null,['class'=>'form-control', 'required'=>true])!!}
	</div>

				
	<div class="input-group">
		<div class="input-group-addon">Category</div>
		{!!Form::select('category',['mortgage'=>'mortgage', 'fullpayment'=>'plot sale', 'rent'=>'Rent', 'misc'=>'Others'], null, array('class'=>'form-control' ));!!}
	</div>


</div>

<hr>


<div class="form-group">
	<label class="control-label">
      Payment Details:
    </label>
    <hr>


	<div class="input-group">
		<div class="input-group-addon">Total </div>
		{!!Form::text('total',null,['class'=>'form-control', 'required'=>true])!!}
	</div>

	<div class="input-group">
		<div class="input-group">
		    <div class="lineputs">
		    <span class="titlehd">Date :</span>
                <span>
                    {!!Form::number('f[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
                    <label for="">DD</label>
                </span>
                <span> - </span>
                <span>
                    {!!Form::number('f[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1, 'required'=>true])!!}
                    <label for="">MM</label>
                </span>
                <span> - </span>
                <span>
                {!!Form::number('f[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900, 'required'=>true])!!}
                    <label for="">YYYY</label>
                </span>
		    </div>
		</div>

	</div>

</div>


<div class="modal-footer">
@if(isset($close) and $close == true)
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
@endif
  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
</div><!-- end of modal footer -->
