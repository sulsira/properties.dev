<header>
	    <nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	            <div class="navbar-brand" id="comenu">
	           <a href="#" id="company_logo">
	             <span>Company</span>
	            </a>
	            <a href="#" id="triger"><span class="octicon octicon-three-bars"></span></a>                   
	            </div>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	            <ul class="nav navbar-nav">
	                    @if($header)
	                        @foreach($header as $section => $nav)
	                            @if(strtolower($section) == 'pages')
                                    @if(strtolower($section) == 'pages')
                                        @foreach($nav as $page => $link )
                                            @foreach($link as $index => $pagina)
                                                @if( (int)$pagina['visible'] )
                                                    @if(  $page == 'pages'  )
                                                        <li class="<?php echo ( strtolower($pagina['name']) == basename($target) or strtolower($pagina['name']) == 'dashboard')? 'active' : '';?>">
                                                        <a href="{!! url($pagina['url']) !!}">{{ ucwords($pagina['name']) }}</a></li>
                                                    @endif

                                                    @if(  $page == 'dropdowns'  )

                                                          @if(isset($pagina['menu']) and !empty($pagina['name']) and $pagina['name'] != 'User')
                                                              <li class="dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                                    {{ ucwords($pagina['name']) }} <span class="caret"></span>
                                                                  </a>
                                                                <ul class="dropdown-menu" role="menu">

                                                                      @foreach($pagina['menu'] as $m => $val)
                                                                          @if((int)$val['visible'])
                                                                              @if( $val['security'] >= $user_details['security_level'] )
                                                                                  <li><a href="#">{{ucwords($val['name'])}}</a></li>
                                                                                   <li class="divider"></li>
                                                                              @endif
                                                                          @endif
                                                                      @endforeach

                                                                </ul>
                                                              </li>
                                                          @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif
                                @endif

                            @endforeach
	                    @endif

	            </ul>
	            @if($user_details and !empty($header))

                    @foreach( array_get($header, 'pages.dropdowns') as $j => $m )

                        @if($m['name'] == 'User')
                            <ul class="nav navbar-nav  navbar-right" id="userdrop">
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                      {!! HTML::image('img/login_bg.jpg','User',["class"=>"img-circle"]) !!} {{ucwords($user_set->role->fullname)}} <span class="caret"></span>
                                    </a>
                                  <ul class="dropdown-menu" role="menu">
                                  <li><a href="#" class="disabled"> {{ucwords($user_set->email)}}</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-header">User Actions</li>
                                    @foreach($m['menu'] as $menu)
                                        @if($menu['visible'])
                                            <li><a href="#">{{ucwords($menu['name'])}}</a></li>
                                        @endif
                                    @endforeach
                                  </ul>
                                </li>
                             </ul>


                        @endif


                    @endforeach

	            @endif
	        </div>
	      </div>
	    </nav>
	</header>