
			<div class="form-group">
					<label class="control-label">
						Invoice:
					</label>
					<hr>

					<div class="input-group">
						<div class="input-group-addon">Description </div>
						{!!Form::text('description',null,['class'=>'form-control', 'required'=>true])!!}
					</div>
        					<div class="input-group">


        						<div class="input-group-addon">Customer</div>
                            {!!Form::select('customers',$customers, null, array('class'=>'select-estate form-control' ));!!}
        					</div>
        					<div class="input-group">
        						<a href="#">add customer</a>

        					</div>
        					

			</div>

				<hr>

				<div class="form-group">
					<label class="control-label">
		              Payment Details:
		            </label>
	            <hr>
					<div class="input-group">
						<div class="input-group-addon">product / service </div>
						{!!Form::text('product',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Total </div>
						{!!Form::text('total',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Quantity </div>
						{!!Form::text('quantity',null,['class'=>'form-control'])!!}
					</div>
				<hr>

							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Due Date :</span>
				                    <span>
				                        {!!Form::number('d[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::number('d[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('d[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>	
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Issued Date :</span>
				                    <span>
				                        {!!Form::number('i[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::number('i[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('i[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>


				</div>
				<div class="modal-footer">
				@if(isset($close))
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				@endif
				  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
				</div><!-- end of modal footer -->
