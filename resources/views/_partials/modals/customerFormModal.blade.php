<div class="modal-dialog" role="document">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">{{$modalTitle}}</h4>
          </div>
          <div class="modal-body">
            @include('_partials/customerForm')
          </div>

        </div>
    </div>
</div>

