
			<div class="form-group">
					<label class="control-label">
						Fullname:
					</label>
					<hr>
					<div class="input-group">
						<div class="input-group-addon">First </div>
						{!!Form::text('pers_fname',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Middle </div>
						{!!Form::text('pers_mname',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Last </div>
						{!!Form::text('pers_lname',null,['class'=>'form-control'])!!}
					</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="control-label">Bio Info:</label>
				<hr>
			    <div class="sub">
				    <div class="lineputs">
				    <span class="titlehd">Birthday:</span>
	                    <span>
	                        {!!Form::number('day',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}

	                        <label for="">DD</label>
	                    </span>
	                    <span> - </span>
	                    <span>
	                        {!!Form::number('month',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
	                        <label for="">MM</label>
	                    </span>
	                    <span> - </span>
	                    <span>
	                        {!!Form::number('year',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
	                        <label for="">YYYY</label>
	                    </span>
				    </div>			    	
			    </div>

                <div class="sub">
                				<div class="input-group">
                					<div class="input-group-addon">Gender </div>
                					{!!Form::select('pers_gender',['male'=>'male','female'=>'female'],null,['class'=>'form-control'])!!}
                				</div>
                				<div class="input-group">
                					<div class="input-group-addon">Nationality </div>
                					{!!Form::select('pers_nationality', $countries, null, array('class'=>'select-estate form-control'));!!}
                				</div>
                </div>

			</div>
			<hr>
			<div class="form-group">
					<label class="control-label">
						Contacts:
					</label>
					<hr>
					<div class="input-group">
						<div class="input-group-addon">Phones </div>
						{!!Form::text('contact[phones]',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Emails </div>
						{!!Form::text('contact[emails]',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Address </div>
						{!!Form::text('contact[address]',null,['class'=>'form-control'])!!}
					</div>
			</div>
			<hr>


				<div class="form-group">
		            <label class="control-label">
		              Plot Details:
		            </label>
		            <hr>
		            <div class="form-group clearfix" id="plot-estate-details">

							<div class="input-group">
        						<div class="input-group-addon">Plot </div>
        						{!!Form::select('plot_id', $plots, null, array('class'=>'select-estate form-control'));!!}
        					</div>

        					<div class="input-group">
        						<div class="input-group-addon">Customer Type</div>
                            {!!Form::select('customerType',['mortgage'=>'mortgage', 'fullpayment'=>'fullpayment'], null, array('class'=>'select-estate form-control' , 'id'=>'customerType'));!!}
        					</div>

		            </div>

				</div>

				<hr>

				<div class="form-group">
					<label class="control-label">
		              Payment Details:
		            </label>
		            <hr>
		            <div id="dropper">
		            	<div class="fullpayment">
		            	
							<div class="input-group">
								<div class="input-group-addon">Amount Paid </div>
								{!!Form::text('amountPaid',null,['class'=>'form-control'])!!}
							</div>

{{-- 							<div class="input-group">
								<div class="input-group-addon">Payment Date</div>
								{!!Form::text('dateOfPayment',null,['class'=>'form-control','placeholder'=>'mm-dd-year,eg: 1-28-2014. all in numbers'])!!}

							</div> --}}
							{{-- <p class="help-block">mm-dd-year,eg: 1-28-2014. all in numbers</p>						 --}}	
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Date of Payment :</span>
				                    <span>
				                        {!!Form::number('f[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::number('f[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('f[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>


		            	</div>
		            	<div class="mortgage">

							<div class="input-group">
								<div class="input-group-addon">Down Payment </div>
								{!!Form::text('downPayment',null,['class'=>'form-control'])!!}
							</div>

							<div class="input-group">
								<div class="input-group-addon">Due Payment </div>
								{!!Form::text('duePayment',null,['class'=>'form-control'])!!}
							</div>
	

							<div class="input-group">
								<div class="input-group-addon">Period In Months</div>
								{!!Form::text('paymentPeriodInMonths',null,['class'=>'form-control'])!!}
								{{--<p class="help-block">Payment in months</p>--}}
							</div>

							<div class="input-group">
								<div class="input-group-addon">Payment Commencement Date</div>
								{!!Form::text('paymentCommencementDate',null,['class'=>'form-control','placeholder'=>'mm-dd-year,eg: 1-28-2014. all in numbers'])!!}

							</div>
							{{-- <p class="help-block">mm-dd-year,eg: 1-28-2014. all in numbers</p>						 --}}			            		

{{-- 							<div class="input-group">
								<div class="input-group-addon">Date of Payment </div>
								{!!Form::text('date_Of_Payment',null,['class'=>'form-control'])!!}
							</div> --}}	
							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd">Date of Payment :</span>
				                    <span>
				                    {!!Form::number('m[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('m[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::number('m[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>

		            	</div>

		            </div>
					
				</div>
				<hr>

				<div class="form-group">
					<label class="control-label">
		              Indentification Details:
		            </label>
		            <hr>
					<div class="input-group">
						<div class="input-group-addon">National ID / Passport # </div>
						{!!Form::text('pers_indentifier',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<label for="exampleInputFile">Photo</label>
						{!!Form::file('photo')!!}
						<p class="help-block">Only jpeg,jpg and png. less than 5mb</p>
					</div>
					<div class="input-group">
						<label for="exampleInputFile">Land Document</label>
						{!!Form::file('document')!!}
						<p class="help-block">Only doc,docx and pdf. less than 5mb</p>
					</div>
				</div>
				<hr>
			<div class="form-group">
					<label class="control-label">
						Next of Kin:
					</label>
					<hr>
					<div class="input-group">
						<div class="input-group-addon">Fullname </div>
						{!!Form::text('fullname',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Contacts </div>
						{!!Form::text('contacts',null,['class'=>'form-control'])!!}
					</div>

			</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
				</div><!-- end of modal footer -->
