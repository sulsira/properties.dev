				<div class="form-group">
		            <label class="control-label">
		              Estate Details:
		            </label>
		            <hr>
		            <div class="form-group clearfix" id="plot-estate-details">
							<div class="input-group">
        						<div class="input-group-addon">Estate </div>
        						{!!Form::select('estate_id', $estates, $estate_id, array('class'=>'select-estate form-control'));!!}
        					</div>

        					{{--<div class="input-group">--}}
        						{{--<div class="input-group-addon">Customer </div>--}}
        						{{--{!!Form::select('plot_customer_id', $customers, $plot_customer_id, array('class'=>'select-estate form-control'));!!}--}}
        					{{--</div>--}}
        					{{--<div class="input-group">--}}
        						{{--<div class="input-group-addon">Agent </div>--}}
        						{{--{!!Form::select('agent_id', $agents, $agent_id, array('class'=>'select-estate form-control'));!!}--}}
        					{{--</div>--}}

		            </div>

				</div>

				<hr>

				<div class="form-group">
					<label class="control-label">
		              Plot Details:
		            </label>
		            <hr>
					<div class="input-group">
						<div class="input-group-addon">Plot Indentification </div>
						{!!Form::text('indentification',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Size </div>
						{!!Form::text('size',null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">Price </div>
						{!!Form::text('price',null,['class'=>'form-control'])!!}
					</div>
				</div>
				<hr>

				<div class="form-group">
					<label class="control-label">
		              Plot Status:
		            </label>
		            <hr>
					<div class="input-group">
						<div class="input-group-addon">Availability </div>
						{!!Form::select('availability',$availability,null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Purchase Type </div>
						{!!Form::select('purchaseType',$purchaseType,null,['class'=>'form-control'])!!}
					</div>
					<div class="input-group">
						<div class="input-group-addon">remarks </div>
						{!!Form::text('remarks',null,['class'=>'form-control'])!!}
					</div>
				</div>

				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
				</div><!-- end of modal footer -->    