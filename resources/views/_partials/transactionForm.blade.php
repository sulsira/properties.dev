
			<div class="form-group">
					<label class="control-label">
						Transaction:
					</label>
					<hr>
					<div class="input-group">
						<div class="input-group-addon">Title </div>
						{!!Form::text('title',null,['class'=>'form-control'])!!}
					</div>

					<div class="input-group">
						<div class="input-group-addon">Description </div>
						{!!Form::text('description',null,['class'=>'form-control'])!!}
					</div>

        					<div class="input-group">
        						<div class="input-group-addon">Revenue</div>
						{!!
							Form::select( 

								'revenue',

								['income'=>'Income', 'expense'=>'Expense'],

								null,

								array(
									'class'=>'form-control',
									'id'=>'revenue'
									)

								);
						!!}
        					</div>
        					
        					<div class="input-group">
        						<div class="input-group-addon">Account</div>
						{!!
							Form::select( 

								'account',

								['bank'=>'Bank', 'cheque'=>'Cheque', 'cash'=>'Cash'],

								null,

								array(
									'class'=>'dropper-selector form-control',
									'id'=>'account'
									)

								);
						!!}
        					</div>


        			<div class="selector-container">
        				<div class="cash" style="display:none">
        									<div class="input-group">
						<div class="input-group-addon">Receipt # </div>
						{!!Form::text('receipt_number',null,['class'=>'form-control','placeholder'=>'receipt or cheque #'])!!}
					</div>	
        				</div>

        				<div class="cheque" style="display:none">
        					<div class="input-group">
						<div class="input-group-addon">Cheque # </div>
						{!!Form::text('cheque_reference',null,['class'=>'form-control','placeholder'=>'receipt or cheque #'])!!}
					</div>
					        					<div class="input-group">
        						<div class="input-group-addon">Bank Account</div>
            {!!Form::select('bank_number',$banks, null, array('class'=>'form-control' ));!!}
        					</div>
					
        				</div>

        				<div class="bank">
					        					<div class="input-group">
        						<div class="input-group-addon">Bank Account</div>
                            {!!Form::select('bank_account',$banks, null, array('class'=>'select-estate form-control'));!!}

        					</div>
        				</div>
        			</div> <!-- end of selector container -->

			</div>
			<hr>
				<div class="form-group">
		            <label class="control-label">
		              Customer Details:
		            </label>
		            <hr>
		            <div class="form-group clearfix" id="plot-estate-details">
        					<div class="input-group">
        						<div class="input-group-addon">Category</div>
                            {!!Form::select('category',['others'=>'Others','mortgage'=>'mortgage', 'fullpayment'=>'plot sale', 'rent'=>'Rent'], null, array('class'=>'dropper-selector select-estate form-control'));!!}
        					</div>
        					<div class="input-group">
        						<div class="input-group-addon">Customer</div>
        						{{--{{dd($customers)}}--}}
                            {!!Form::select('customer',array_add($customers,'0','select customer'), 0, array('class'=>'select-estate form-control'));!!}
        					</div>
					<div class="input-group">
						<div class="input-group-addon">Total </div>
						{!!Form::text('total',null,['class'=>'form-control'])!!}
					</div>
		            </div>

				</div>

				<hr>

				<div class="form-group">
					<label class="control-label">
		              Payment Details:
		            </label>
		            <hr>
		            <div class="selector-container">
		            	<div class="fullpayment" style="display:none">
{{-- 		            								<div class="input-group">
        						<div class="input-group-addon">Plot </div>
        						{!!Form::select('plot_id', $plots, null, array('class'=>'select-estate form-control'));!!}
        					</div> --}}

							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd"> Date :</span>
				                    <span>
				                        {!!Form::input('number','f[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::input('number','f[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::input('number','f[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>
		            	</div>
		            	<div class="mortgage" style="display:none">
{{-- 							<div class="input-group">
        						<div class="input-group-addon">Plot </div>
        						{!!Form::select('plot_id', $plots, null, array('class'=>'select-estate form-control'));!!}
        					</div> --}}


							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd"> Date :</span>
				                    <span>
				                        {!!Form::input('number','p[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::input('number','p[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::input('number','p[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>
		            	</div>
		            	<div class="rent" style="display:none">

							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd"> Date :</span>
				                    <span>
				                        {!!Form::input('number','r[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::input('number','r[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::input('number','r[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>

		            	</div>
		            	<div class="others">

							<div class="input-group">
								<div class="input-group-addon">For </div>
								{!!Form::text('for',null,['class'=>'form-control'])!!}
							</div>


							<div class="input-group">
							    <div class="lineputs">
							    <span class="titlehd"> Date :</span>
				                    <span>
				                        {!!Form::input('number','o[day]',null, ['class'=>"smallest", 'step'=>1, 'max'=>31, 'min'=>1])!!}
				                        <label for="">DD</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                        {!!Form::input('number','o[m]',null, ['class'=>"smallest", 'step'=>1, 'max'=>12, 'min'=>1])!!}
				                        <label for="">MM</label>
				                    </span>
				                    <span> - </span>
				                    <span>
				                    {!!Form::input('number','o[y]',null, ['class'=>"smaller", 'step'=>1, 'max'=>2090, 'min'=>1900])!!}
				                        <label for="">YYYY</label>
				                    </span>
							    </div>
				    		</div>

		            	</div>

		            </div>

				</div>
				<hr>
					<div class="input-group">
						<label for="exampleInputFile">Upload Support Document</label>
						{!!Form::file('upload')!!}
						<p class="help-block">Only jpeg,jpg and png. less than 5mb</p>
					</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  <button type="submit" class="btn btn-primary">{{$submitButtonText}}</button>
				</div><!-- end of modal footer -->
