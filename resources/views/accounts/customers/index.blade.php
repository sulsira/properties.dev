
<?php 
 // dd($data['PlotsCustomer']->toArray());
 ?>
@include('templates/account')
  <div class="main cc">  
    <div class="panel panel-info">
      <div class="panel-heading clearfix">
          <h3 class="panel-title">Customers list</h3>
            <div class="btn-group fiet">
                        <a href="" class="sbm dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                          <span class="octicon octicon-three-bars"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        <li class="dropdown-header">Display</li>
                          <li><a href="#">all</a></li>
                          <li><a href="#">Rent</a></li>
                          <li><a href="#">land loards</a></li>
                          <li><a href="#">Estates customers</a></li>
                          <li><a href="#">plot fullpayment</a></li>
                          <li><a href="#">plot mortgage</a></li>
                          <li class="dropdown-header">Sort by</li>
                          <li><a href="#">Department</a></li>
                          <li><a href="#">Year</a></li>
                          <li class="divider"></li>
                          <li><a href="#">add new customer</a></li>
                          <li><a href="#">print list</a></li>
                        </ul>
            </div>
       </div>
       <div class="panel-body">

				<div class="table-responsive">
            @include('_partials/data/customersTable')
				</div>

        
       </div>
    </div>
  </div> <!-- main cc -->
@include('templates/account_end')
