@include('templates/account')
    <div class="main cc">  
        <div class="panel panel-info">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">Create An Invoice</h3>
                <div class="btn-group fiet">
                  <a href="#" class="sbm dropdown-toggle" >
                    <span class="octicon octicon-plus"></span>
                  </a>
                </div>
             </div>
             <div class="panel-body">
             @include("flash::message")
				<div class="table-responsive">
					@include('_partials/errors')
		            {!!Form::open(array('route'=>'accounts.invoices.store','class'=>"form-inline",'id'=>"create_invoice"))!!}
                        @include('_partials/invoiceForm',['submitButtonText'=>'Add Invoice', 'close'=>true])
		            {!!Form::close()!!}
				</div>
              </div>
        </div>
    </div> <!-- main cc -->

@include('templates/account_end')