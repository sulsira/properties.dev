@include('templates/estate')
    <div class="main cc">  

        <div class="panel panel-info">
              <div class="panel-heading clearfix">
                <h3 class="panel-title">Estates Table</h3>
                <div class="btn-group fiet">
                  <a href="" class="sbm dropdown-toggle" class="" type="button" data-toggle="dropdown" aria-expanded="false">
                    <span class="octicon octicon-three-bars"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li><a href="#">create a user</a></li>
                    <li><a href="#">block users</a></li>
                    <li><a href="#">Logout users</a></li>
                    <li><a href="#">download user list</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Delete</a></li>
                  </ul>
                </div>
              </div>
              <div class="panel-body">
                {{--@include('_partials/flash')--}}
                @include("flash::message")
				<div class="table-responsive">

		            {!!Form::open(array('route'=>'estates.store','class'=>"form-inline",'id'=>"user_form"))!!}
		                @include('_partials/estateForm',['submitButtonText'=>'Add Estate'])
		            {!!Form::close()!!}
		            @if($errors->any())
		                <ul class="alert alert-danger">
		                    @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
		                    @endforeach
		                </ul>
		            @endif
				</div>
              </div>
              <hr>
              <div class="line">  
                  <a href="#create_user" data-toggle="modal" data-target="#create_user"><span class="octicon octicon-plus"></span></a> |
                  <a href="#"><span class="octicon octicon-checklist"></span></a> |
                  <a href="#"><span class="octicon octicon-issue-reopened"></span></a> |
                  <a href="#"><span class="octicon octicon-lock"></span></a> |
                  <a href="#"><span class="octicon octicon-database"></span></a> 
              </div>  
            </div>

    </div> <!-- main cc -->
@include('templates/estate_end')