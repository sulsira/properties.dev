{!!
 	Form::open(array('route'=>'estates.store',
	 'class'=>"modal fade",
	  'id'=>"estate_add",
	  'tabindex'=>"-1",
	  'role'=>"dialog",
	   'aria-labelledby'=>$modalID.'ModalLabel',
	   'data-id'=> $modalID
	 ))
 !!}
	@include('_partials/modals/estateFormModal',['modalID'=> $modalID ,'modalTitle'=>'create a new estate','submitButtonText'=>'Create Estate'])
{!!Form::close()!!}           





