
    <footer>
        <div class="container">
            <ul>
                <li>2014 &ndash; <?php echo date('Y'); ?> @ copyright</li>
                <li><a href="#">developers</a></li>
                <li><a href="#">privacy</a></li>
            </ul>
        </div>
    </footer>
<!-- scripts -->
{!! HTML::script('js/jquery-1.11.1.js')!!}
{!!HTML::script('js/bootstrap.js')!!}
{!!HTML::script('js/popsub.js')!!}
{!!HTML::script('js/sidebarToggle.js')!!}
{!!HTML::script('js/sidebarslider.js')!!}
{!!HTML::script('js/jquery.ajax-progress.js')!!}
{!!HTML::script('js/ModalLoader.query.js')!!}
{!!HTML::script('js/select2.full.min.js')!!}
{!!HTML::script('js/selectdrop.js')!!}
{!!HTML::script('js/pusher/pusher.js')!!}
{!!HTML::script('js/app.js')!!}
{!!HTML::script('js/jquery.dropper.js')!!}
{!!HTML::script('js/jquery.dataTables.min.js')!!}
{!!HTML::script('js/dataTables.bootstrap.min.js')!!}
<script>
    $('a#triger').sidebarSlider();
        $('#checker').popover();
    $('#checker').on('click', function(){
      $("#check").slideToggle(258);
    });
$('nav.add-nav').modalLoader();
//    $('nav.add-nav').modalLoader().done(function(){
//    console.log("you are did it");
//    });

    $('div.alert').not('.alert-important').delay(3000).slideUp(300);

    $('#flash-overlay-modal').modal();

    $(document).ready(function() {

      $(".select-estate").select2();

      $.subscribe('modal/loaded', function(){
        $(".select-estate").select2();
      });

    });

    $('form').dropper();
$(document).ready(function(){
    $('.datatify').DataTable();
});
</script>
