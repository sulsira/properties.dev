@include('inc/doc')
<body class="dashboard">
	@yield('header')
	@yield('sidebar')
	@yield('container')
	@section('footer')
	<div class="platform"></div>
		@parent
	@stop
</body>
</html>
