@include('inc/doc')
<body class="login"> <!-- doc zone -->
		<div class="container">
			@yield('header')
			@yield('content')
		</div>
		@section('footer')
			@parent
		@stop
</body>
</html>
