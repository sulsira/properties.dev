@include('templates/estate')
    <div class="main cc">  
        <div class="panel panel-info">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">Create Plots</h3>
                <div class="btn-group fiet">
                  <a href="#" class="sbm dropdown-toggle" >
                    <span class="octicon octicon-plus"></span>
                  </a>
                </div>
             </div>
             <div class="panel-body">
             @include("flash::message")
				<div class="table-responsive">
					@include('_partials/errors')
		            {!!Form::open(array('route'=>'plots.store','class'=>"form-inline",'id'=>"create_plot"))!!}
		                @include("_partials/plotForm",['submitButtonText'=>'Add Plot'])
		            {!!Form::close()!!}
				</div>
              </div>
        </div>
    </div> <!-- main cc -->
@include('templates/estate_end')