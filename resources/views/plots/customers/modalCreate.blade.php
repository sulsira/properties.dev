{!!
 	Form::open(array('route'=>'plots.customers.store',
	 
	 'class'=>"modal fade",
	  'id'=>"plotcustomer_add",
	  'role'=>"dialog",
	   'aria-labelledby'=>"myLargeModalLabel",
	   'data-id'=> $modalID
	 ))
 !!}
	@include('_partials/modals/plotCustomerFormModal',['modalID'=> $modalID ,'modalTitle'=>'Add new plot customer','submitButtonText'=>'Create Plot Customer'])
{!!Form::close()!!}           
