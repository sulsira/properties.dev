{!!
 	Form::open(array('route'=>'plots.store',
	 
	 'class'=>"modal fade",
	  'id'=>"plot_add",
	  'role'=>"dialog",
	   'aria-labelledby'=>"myLargeModalLabel",
	   'data-id'=> $modalID
	 ))
 !!}
	@include('_partials/modals/plotFormModal',['modalID'=> $modalID ,'modalTitle'=>'create a new plot','submitButtonText'=>'Create Plot'])
{!!Form::close()!!}           
