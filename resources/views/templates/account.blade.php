@extends('layouts.dashboard')
@section('styles')
    @parent
 	{!! HTML::style('css/scheme.css') !!}
@stop
@section('header')
    @include('_partials/header')
@stop
@section('sidebar')
    <div class="col-sm-3 col-md-2 sidebar">
        @include('_partials/sidebar')

		  <ul class="nav nav-sidebar" id="snav">
		    <li>
		        <a href="#">
		            <span class="octicon octicon-organization"></span>
		            <span>Statements</span>
		        </a>
		    </li>
		    <li>
		        <a href="">
		            <span class="octicon octicon-location"></span>
		            <span>Revenue</span>
		        </a>
		      </li>
		    <li>
		        <a href="">
		            <span class="octicon octicon-diff-ignored"></span>
		            <span>Income</span>
		        </a>
		      </li>
		    <li>
		        <a href="">
		            <span class="octicon octicon-unfold"></span>
		            <span> Expense</span>

		        </a>
		      </li>
		  </ul>
		  @if(isset($header))
            @if( array_get($header, 'system') )
		        <ul class="nav nav-sidebar" id="systems">
		  		    <li>
		  		        <a href="">
		  		        	<span class="octicon octicon-versions"></span> <span>Systems</span>
          		    	</a>
          		    </li>
                    @foreach( array_get( $header, 'system' ) as $sys => $system)
                                <li class="system">
                                    <a href="">
                                        <span class="octicon octicon-diff-ignored"></span>
                                        <span>{{ucwords($sys)}}</span>
                                    </a>
                                  @if(isset($system) and !empty($system))
                                     <ul class="nav nav-sidebar system-menu">
                                        @foreach($system as $menu)
                                           @if((int)$menu['visible'])
                                                <li>
                                                    <a href="">
                                                        <span class="octicon octicon-unfold"></span>
                                                        <span> {{ucwords($menu['name'])}}</span>

                                                    </a>
                                                </li>
                                           @endif
                                        @endforeach
                                     </ul>

                                  @endif
                                </li>
                    @endforeach
		        </ul>
            @endif
		  @endif
		  <ul class="nav nav-sidebar">
		    <li><a href=""><span class="octicon octicon-versions"></span> <span>
		        Menu view</span></a></li>
		  </ul>
    </div>
@stop
@section('container')

	<div class="main_content col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
	    @include('_partials/fetch')
	    @if(str_contains($user_details['previledges'], 'a'))
           <nav class="add-nav breadcrumbs">
                <ol class="breadcrumb">
                <li <?php echo ($target == 'accounts/transactions/create')? ' class="active" ': '';?>>
                      <a href="{{route('accounts.transactions.create')}}">
                        <i class="octicon octicon-plus"></i>
                         Transaction
                      </a>
                  </li>
                  <li <?php echo ($target == 'accounts/customers/create')? ' class="active" ': '';?>>
                      <a href="{{route('accounts.customers.create')}}">
                        <i class="octicon octicon-plus"></i>
                         Customer
                      </a>
                  </li>
                  <li <?php echo ($target == 'accounts/invoices/create')? ' class="active" ': '';?>>
                      <a href="{{route('accounts.invoices.create')}}" class="-adder">
                            <i class="octicon octicon-plus"></i>
                            Invoice
                       </a>
                  </li>
                  <li <?php echo ($target == 'accounts/receipts/create')? ' class="active" ': '';?>>
                      <a href="{{url('accounts/receipts/create')}}" class="-adder">
                          <i class="octicon octicon-plus"></i>
                           Receipt
                      </a>
                  </li>
                  <li <?php echo ($target == 'accounts/incomes/create')? ' class="active" ': '';?>>
                      <a href="{{url('accounts/incomes/create')}}" class="-adder">
                          <i class="octicon octicon-plus"></i>
                           Income
                      </a>
                  </li>
                  <li <?php echo ($target == 'accounts/expenses/create')? ' class="active" ': '';?>>
                      <a href="{{url('accounts/expenses/create')}}" class="-adder">
                          <i class="octicon octicon-plus"></i>
                           Expense
                      </a>
                  </li>

                </ol>
            </nav>
        @endif
	    @include('_partials/loader')
	    <div class="main cc">
@stop
