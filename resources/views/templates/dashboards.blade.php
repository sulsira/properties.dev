@extends('layouts.dashboard')
@section('styles')
    @parent
 	{!! HTML::style('css/scheme.css') !!}
@stop
@section('header')
    @include('_partials/header')
@stop
@section('sidebar')
    @include('_partials/sidebar')
@stop
@section('container')
	<div class="main_content col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
	    @include('_partials/fetch')
	    @include('_partials/breadcrumb')
	    @include('_partials/loader')
	    <div class="main cc">
@stop

