@extends('layouts.dashboard')
@section('styles')
    @parent
 	{!! HTML::style('css/scheme.css') !!}
@stop
@section('header')
    @include('_partials/header')
@stop
@section('sidebar')
    <div class="col-sm-3 col-md-2 sidebar">
        @include('_partials/sidebar')

		  <ul class="nav nav-sidebar" id="snav">
		    <li>
		        <a href="#">
		            <span class="octicon octicon-organization"></span>
		            <span>customers</span>
		        </a>
		    </li>
		    <li>
		        <a href="">
		            <span class="octicon octicon-location"></span>
		            <span>plots</span>
		        </a>
		      </li>
		    <li>
		        <a href="">
		            <span class="octicon octicon-diff-ignored"></span>
		            <span>compounds</span>
		        </a>
		      </li>
		    <li>
		        <a href="">
		            <span class="octicon octicon-unfold"></span>
		            <span> transactions</span>

		        </a>
		      </li>
		  </ul>
		  @if(isset($header))
            @if( array_get($header, 'system') )
		        <ul class="nav nav-sidebar" id="systems">
		  		    <li>
		  		        <a href="">
		  		        	<span class="octicon octicon-versions"></span> <span>Systems</span>
          		    	</a>
          		    </li>
                    @foreach( array_get( $header, 'system' ) as $sys => $system)
                                <li class="system">
                                    <a href="">
                                        <span class="octicon octicon-diff-ignored"></span>
                                        <span>{{ucwords($sys)}}</span>
                                    </a>
                                  @if(isset($system) and !empty($system))
                                     <ul class="nav nav-sidebar system-menu">
                                        @foreach($system as $menu)
                                           @if((int)$menu['visible'])
                                                <li>
                                                    <a href="">
                                                        <span class="octicon octicon-unfold"></span>
                                                        <span> {{ucwords($menu['name'])}}</span>

                                                    </a>
                                                </li>
                                           @endif
                                        @endforeach
                                     </ul>

                                  @endif
                                </li>
                    @endforeach
		        </ul>
            @endif
		  @endif
		  <ul class="nav nav-sidebar">
		    <li><a href=""><span class="octicon octicon-versions"></span> <span>
		        Menu view</span></a></li>
		  </ul>
    </div>
@stop
@section('container')
	<div class="main_content col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
	    @include('_partials/fetch')
	    @if(str_contains($user_details['previledges'], 'a'))
           <nav class="add-nav breadcrumbs">
                <ol class="breadcrumb">
                  <li <?php echo ($target == 'estates/create')? ' class="active" ': '';?>>
                      <a href="{{route('estates.create')}}" class="adder" data-toggle="modal" data-target="#estate_add" data-whatever="@mdo">
                        <i class="octicon octicon-plus"></i>
                         Estates
                      </a>
                  </li>
                  <li <?php echo ($target == 'plots/create')? ' class="active" ': '';?>>
                      <a href="{{route('plots.create')}}" class="-adder">
                            <i class="octicon octicon-plus"></i>
                            Plots
                       </a>
                  </li>
                  <li <?php echo ($target == 'plots/customers/create')? ' class="active" ': '';?>>
                      <a href="{{url('plots/customers/create')}}" class="-adder">
                          <i class="octicon octicon-plus"></i>
                           Customers
                      </a>
                  </li>

                </ol>
            </nav>
        @endif
	    @include('_partials/loader')
	    <div class="main cc">
@stop
